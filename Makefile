install:
	cp -n .env.orig .env
	cp -n docker-compose.yml.dev docker-compose.yml
	make pull
	make build
	make up
	make vendor
	make migrate
.PHONY: install

build:
	docker-compose build
.PHONY: build

pull:
	docker-compose pull
.PHONY: pull

up:
	docker-compose up -d
.PHONY: up

down:
	docker-compose down
.PHONY: down

vendor:
	docker-compose exec -T phpfpm sh -c "composer install --no-cache"
.PHONY: vendor

create-buckets:
	docker-compose exec -T phpfpm sh -c "bin/console common:create:buckets"
.PHONY: create-buckets

migrate:
	docker-compose exec -T phpfpm sh -c "bin/console doctrine:migrations:migrate --no-interaction"
.PHONY: migrate

diff:
	docker-compose exec -T phpfpm sh -c "bin/console doctrine:cache:clear-metadata"
	docker-compose exec -T phpfpm sh -c "bin/console doctrine:migrations:diff"
.PHONY: diff

reload:
	docker-compose exec -T phpfpm sh -c "php bin/console doctrine:database:drop --force"
	docker-compose exec -T phpfpm sh -c "php bin/console doctrine:database:create"
.PHONY: reload

clear:
	docker-compose exec -T phpfpm sh -c "composer dump-autoload --no-cache"
	docker-compose exec -T phpfpm sh -c "bin/console cache:clear"
.PHONY: clear


