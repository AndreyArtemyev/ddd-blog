<?php

declare(strict_types=1);

namespace App\Blog\Domain\ReadModel;

use App\Common\Lib\Dto\DTO;
use App\Common\Lib\Helpers\DateTimeHelper;
use DateTimeImmutable;
use OpenApi\Annotations as OA;
use Nelmio\ApiDocBundle\Annotation\Model;
use Ramsey\Uuid\UuidInterface;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class PostList extends DTO
{
    /**
     * @OA\Property(property = "uuid", type = "string",
     *  example = "e8fa6767-d8c3-8a46-ad59-7c2ebd1f6067",
     *  description="Уникальный идентификатор поста"
     * )
     */
    public UuidInterface $uuid;

    /**
     * @OA\Property(property = "alias", type = "string", nullable = false,
     *     description = "Название статьи", example = "Пример названия"
     * )
     */
    public string $alias;

    /**
     * @OA\Property(property = "body", type = "string", nullable = false,
     *     description = "Текст статьи", example = "Пример текста"
     * )
     */
    public string $body;

    /**
     * @OA\Property(property = "alias_picture_url", type = "string", nullable = false,
     *     description = "Ссылка на картинку для статьи", example = "http://example.com"
     * )
     */
    public ?string $alias_picture_url = null;

    /**
     * @OA\Property(property = "date_posted", type = "string", nullable = false,
     *  example = "1990-11-05", description="Дата публикации статьи",
     * )
     */
    public DateTimeImmutable $date_posted;

    public function __construct(array $data = [])
    {
        if (!empty($data['date_posted'])) {
            $data['date_posted'] = new DateTimeImmutable((string)$data['date_posted']);
        }
        if (!empty($data['alias_picture_url'])) {
            /** TODO сделать возврат урла по уиду картинки */
            $data['alias_picture_url'] = null;
        }
        parent::__construct($data);
    }

    public function jsonSerialize(): array
    {
        $result = parent::jsonSerialize();

        $result['date_posted'] = DateTimeHelper::forFront($this->date_posted);

        return $result;
    }
}
