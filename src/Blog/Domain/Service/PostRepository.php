<?php

declare(strict_types=1);

namespace App\Blog\Domain\Service;

use App\Blog\Domain\Entity\Post;
use App\Blog\Domain\Exception\PostNotFoundException;
use App\Common\Service\Persistence\DoctrineRepository;
use Doctrine\Persistence\ObjectRepository;
use Ramsey\Uuid\UuidInterface;

class PostRepository
{
    private ObjectRepository $objectRepository;
    private DoctrineRepository $doctrineRepository;

    public function __construct(DoctrineRepository $doctrineRepository)
    {
        $this->objectRepository = $doctrineRepository->getObjectRepository(Post::class);
        $this->doctrineRepository = $doctrineRepository;
    }

    public function findOneByUuid(UuidInterface $uuid): ?Post
    {
        return $this->objectRepository->findOneBy(
            [
                'uuid' => $uuid,
            ]
        );
    }

    public function findOneByUuidOrFail(UuidInterface $uuid): Post
    {
        $post = $this->findOneByUuid($uuid);
        if ($post instanceof Post) {
            return $post;
        }

        throw new PostNotFoundException();
    }
}
