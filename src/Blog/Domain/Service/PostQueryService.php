<?php

declare(strict_types=1);

namespace App\Blog\Domain\Service;

use App\Blog\Domain\ReadModel\PostList;
use App\Common\Lib\GlobalDto\FilterCommand;
use App\Common\Lib\GlobalDto\FilterCommandCollection;
use App\Common\Lib\GlobalDto\Pagination;
use App\Common\Lib\Dto\DTOCollection;
use App\Common\Service\Persistence\DoctrineQueryService;
use App\Common\Service\Persistence\DoctrineRepository;
use Doctrine\DBAL\ParameterType;

class PostQueryService
{
    private DoctrineQueryService $doctrineQueryService;
    private DoctrineRepository $doctrineRepository;

    public function __construct(
        DoctrineQueryService $doctrineQueryService,
        DoctrineRepository $doctrineRepository
    ) {
        $this->doctrineQueryService = $doctrineQueryService;
        $this->doctrineRepository = $doctrineRepository;
    }

    /**
     * @param Pagination $pagination
     * @param FilterCommandCollection $filter
     * @return DTOCollection<int, PostList>
     */
    public function getPostList(Pagination $pagination, FilterCommandCollection $filter): DTOCollection
    {
        ['sql' => $sql, 'parameters' => $parameters] = $this->getPostListQuery($pagination, $filter, false);
        return $this->doctrineQueryService->fetchAllDTO($sql, $parameters, PostList::class);
    }

    public function getPostListQuantity(Pagination $pagination, FilterCommandCollection $filter): int
    {
        ['sql' => $sql, 'parameters' => $parameters] = $this->getPostListQuery($pagination, $filter, true);
        return $this->doctrineQueryService->fetchInt($sql, $parameters);
    }

    /**
     * @param Pagination $pagination
     * @param FilterCommandCollection $filter
     * @param bool $forQuantity
     * @return array
     */
    private function getPostListQuery(
        Pagination $pagination,
        FilterCommandCollection $filter,
        bool $forQuantity
    ): array {
        /*
        select posts.uuid as uuid
             , posts.alias as alias
             , posts.body as body
             , posts.alias_picture_uuid as alias_picture_uuid
             , posts.date_posted as date_posted
        from blog_posts posts
        ORDER BY posts.date_posted DESC
        limit :offset, :row_count
         */

        $parameters = [];

        if ($forQuantity) {
            $select = 'count(*) as quantity';
        } else {
            $select = '
               posts.uuid as uuid
             , posts.alias as alias
             , posts.body as body
             , posts.alias_picture_uuid as alias_picture_url
             , posts.date_posted as date_posted
            ';
        }

        if ($forQuantity) {
            $limit = '';
        } else {
            $limit = 'limit :offset, :row_count';
            $parameters = array_merge($parameters, $pagination->generateParamForQuery());
        }

        $whereBuilder = [];
        /** @var FilterCommand $filterCommand */
        foreach ($filter as $filterCommand) {
            switch ($filterCommand->fetchCommand()) {
                case 'search':
                    $whereBuilder[] =
                        'posts.alias like :search_data';
                    $parameters['search_data'] = [
                        'value' => "%{$filterCommand->fetchData()}%",
                        'type' => ParameterType::STRING,
                    ];
                    break;

                default:
                    throw new \Exception('Not allowed');
                    break;
            }
        }

        $where = '';
        if (count($whereBuilder)) {
            $where = 'where (' . implode(') and (', $whereBuilder) . ')';
        }

        $sql = <<<SQL
            select {$select}
            from blog_posts posts
            {$where}
            ORDER BY posts.date_posted DESC
            {$limit}
        SQL;

        return [
            'sql' => $sql,
            'parameters' => $parameters,
        ];
    }
}
