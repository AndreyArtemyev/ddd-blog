<?php

declare(strict_types=1);

namespace App\Blog\Domain\Entity;


use App\Carsharing\Domain\BookingInterval\Entity\BookingIntervalMember;
use App\Common\Lib\Doctrine\AggregateRoot;
use App\Common\Lib\Doctrine\TimestampableImmutableTrait;
use DateTimeImmutable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

/**
 * @ORM\Entity()
 * @ORM\Table(name="blog_posts")
 */
class Post extends AggregateRoot
{
    use TimestampableImmutableTrait;

    /**
     * @var UuidInterface
     * @ORM\Id
     * @ORM\Column(name="uuid", type="uuid", unique=true)
     */
    private UuidInterface $uuid;

    /**
     * @var UuidInterface
     * @ORM\Column(name="user_uuid", type="uuid")
     */
    private UuidInterface $userUuid;

    /**
     * @var string
     * @ORM\Column(name="alias", type="text", nullable=false)
     */
    private string $alias;

    /**
     * @var string
     * @ORM\Column(name="body", type="text", nullable=false)
     */
    private string $body;

    /**
     * @var UuidInterface
     * @ORM\Column(name="alias_picture_uuid", type="uuid", nullable=true)
     */
    private UuidInterface $aliasPictureUuid;

    /**
     * @var DateTimeImmutable
     * @ORM\Column(name="date_posted", type="datetime_immutable", nullable=true)
     */
    private DateTimeImmutable $datePosted;

    /**
     * @var Collection|Comment[]
     * @psalm-var Collection<int, Comment>
     * @ORM\OneToMany(
     *     targetEntity="Comment",
     *     mappedBy="post",
     *     cascade={"all"},
     *     orphanRemoval=true
     *     )
     */
    private $comments;

    public function toArray(): array
    {
        return [
            'uuid' => $this->uuid,
            'userUuid' => $this->userUuid,
            'alias' => $this->alias,
            'body' => $this->body,
            'aliasPictureUuid' => $this->aliasPictureUuid,
            'datePosted' => $this->datePosted,
        ];
    }

    public function jsonSerialize(): array
    {
        return $this->toArray();
    }

    public function getUuid(): UuidInterface
    {
        return $this->uuid;
    }
}
