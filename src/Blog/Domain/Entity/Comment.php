<?php

declare(strict_types=1);

namespace App\Blog\Domain\Entity;


use App\Common\Lib\Doctrine\AggregateRoot;
use App\Common\Lib\Doctrine\TimestampableImmutableTrait;
use DateTimeImmutable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

/**
 * @ORM\Entity()
 * @ORM\Table(name="blog_comments")
 */
class Comment
{
    use TimestampableImmutableTrait;

    /**
     * @var UuidInterface
     * @ORM\Id
     * @ORM\Column(name="uuid", type="uuid", unique=true)
     */
    private UuidInterface $uuid;

    /**
     * @var Post
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="Post", inversedBy="comments")
     * @ORM\JoinColumn(name="post_uuid", referencedColumnName="uuid", nullable=false)
     */
    private Post $post;

    /**
     * @var UuidInterface
     * @ORM\Column(name="user_uuid", type="uuid")
     */
    private UuidInterface $userUuid;

    /**
     * @var string
     * @ORM\Column(name="text", type="text", nullable=false)
     */
    private string $text;

    /**
     * @var UuidInterface
     * @ORM\Column(name="parent_comment_uuid", type="uuid", nullable=true)
     */
    private UuidInterface $parentCommentUuid;

    public function __construct(
        Post $post,
        UuidInterface $userUuid,
        string $text,
        UuidInterface $parentCommentUuid
    ) {
        $this->post = $post;
        $this->userUuid = $userUuid;
        $this->text = $text;
        $this->parentCommentUuid = $parentCommentUuid;
    }
}
