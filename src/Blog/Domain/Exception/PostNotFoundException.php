<?php

declare(strict_types=1);

namespace App\Blog\Domain\Exception;


class PostNotFoundException extends \DomainException
{
    protected $code = DomainExceptionCode::POST_NOT_FOUND;
    protected $message = 'Статья не найдена';
}
