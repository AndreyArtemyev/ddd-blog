<?php

declare(strict_types=1);

namespace App\Blog\Domain\Exception;

class DomainExceptionCode
{
    public const POST_NOT_FOUND = 2001;
}
