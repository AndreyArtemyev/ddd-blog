<?php

declare(strict_types=1);

namespace App\Blog\Application\Command\GetPostList;

use App\Blog\Domain\Service\PostQueryService;
use App\Common\Lib\GlobalDto\FilterCommandCollection;
use App\Common\Lib\GlobalDto\Pagination;
use App\Common\Lib\GlobalDto\PaginationResponse;

class GetPostListHandler
{
    private PostQueryService $postQueryService;

    public function __construct(PostQueryService $postQueryService)
    {
        $this->postQueryService = $postQueryService;
    }

    public function handle(Pagination $pagination, FilterCommandCollection $filter): GetPostListResult
    {
        $posts = $this->postQueryService->getPostList($pagination, $filter);
        $pagination = PaginationResponse::create(
            $pagination,
            $this->postQueryService->getPostListQuantity($pagination, $filter)
        );
        $getPostListResult = GetPostListResult::createResult($posts, $pagination);
        return $getPostListResult;
    }
}
