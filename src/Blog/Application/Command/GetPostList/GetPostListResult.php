<?php

declare(strict_types=1);

namespace App\Blog\Application\Command\GetPostList;

use App\Blog\Domain\ReadModel\PostList;
use App\Common\Lib\Dto\DTO;
use App\Common\Lib\Dto\DTOCollection;
use App\Common\Lib\GlobalDto\PaginationResponse;
use OpenApi\Annotations as OA;
use Nelmio\ApiDocBundle\Annotation\Model;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class GetPostListResult extends DTO
{
    /**
     * @OA\Property(property = "posts", type = "array",
     *  @OA\Items(
     *      ref = @Model(type = PostList::class)
     *  )
     * )
     * @var DTOCollection<int, PostList>
     */
    public DTOCollection $posts;

    /**
     * @OA\Property(property = "pagination", type = "object",
     *  ref = @Model(type = PaginationResponse::class)
     * )
     * @var PaginationResponse
    */
    public PaginationResponse $pagination;

    public static function createResult(
        DTOCollection $posts,
        PaginationResponse $pagination
    ): GetPostListResult {
        try {
            $getPostListResult = new self(
                [
                    'posts' => $posts,
                    'pagination' => $pagination
                ]
            );
        } catch (\Exception $exception) {
            throw new BadRequestHttpException('Invalid data');
        }
        return $getPostListResult;
    }
}
