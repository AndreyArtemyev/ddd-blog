<?php

declare(strict_types=1);

namespace App\Blog\UI\Http\Controller;

use App\Blog\Application\Command\GetPostList\GetPostListHandler;
use App\Blog\Application\Command\GetPostList\GetPostListResult;
use App\Common\Lib\Controller\JsonApiResponse;
use App\Common\Lib\Controller\JsonApiResponseGenerator;
use App\Common\Lib\Dto\DTOFactory;
use App\Common\Lib\GlobalDto\FilterCommand;
use App\Common\Lib\GlobalDto\FilterCommandCollection;
use App\Common\Lib\GlobalDto\Pagination;
use Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Nelmio\ApiDocBundle\Annotation\Operation;
use OpenApi\Annotations as OA;
use Nelmio\ApiDocBundle\Annotation\Model;

final class GetPostListController
{
    protected GetPostListHandler $getPostListHandler;
    private JsonApiResponseGenerator $jsonApiResponseGenerator;

    public function __construct(
        GetPostListHandler $getPostListHandler,
        JsonApiResponseGenerator $jsonApiResponseGenerator
    ) {
        $this->getPostListHandler = $getPostListHandler;
        $this->jsonApiResponseGenerator = $jsonApiResponseGenerator;
    }

    /**
     * @Route("/post/get-list", name = "blog_post_get_list", methods = {"POST"})
     *
     * @Operation(
     *  tags = {"Post"},
     *  summary = "Получить список всех статей",
     *  @OA\RequestBody(
     *      @OA\JsonContent(
     *          type = "object",
     *          required = {
     *              "pagination",
     *              "filter",
     *          },
     *          @OA\Property(property = "filter", type = "array",
     *              @OA\Items(
     *                  ref = @Model(type = FilterCommand::class)
     *              ),
     *          ),
     *          @OA\Property(property = "pagination",
     *              ref = @Model(type = Pagination::class)
     *          ),
     *      )
     *  ),
     *  @OA\Response(
     *      response = "200",
     *      description = "Returned when successful",
     *      @OA\JsonContent(
     *          type = "object",
     *          @OA\Property(property = "success", type = "boolean", example = true),
     *          @OA\Property(property = "error", type = "object", nullable = true, example = null),
     *          @OA\Property(property = "data", type = "object", nullable = false,
     *              ref = @Model(type = GetPostListResult::class),
     *          ),
     *      ),
     *  ),
     * )
     *
     * @param Request $request
     * @return JsonApiResponse
     */
    public function __invoke(Request $request): JsonApiResponse
    {
        $pagination = DTOFactory::createDtoFromData(Pagination::class, $request->request->get('pagination') ?? []);

        $filter = DTOFactory::createDtoCollectionTypedFromData(
            FilterCommandCollection::class,
            $request->request->get('filter') ?? []
        );

        $result = $this->getPostListHandler->handle($pagination, $filter);
        return $this->jsonApiResponseGenerator->success($result);
    }
}
