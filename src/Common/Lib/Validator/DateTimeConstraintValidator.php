<?php

declare(strict_types=1);

namespace App\Common\Lib\Validator;

use Symfony\Component\Validator\Constraints\DateTimeValidator;

class DateTimeConstraintValidator extends DateTimeValidator
{
}
