<?php

declare(strict_types=1);

namespace App\Common\Lib\Validator;

use Symfony\Component\Validator\Constraints\Optional;

/**
 * @Annotation
 * @Target({"PROPERTY", "ANNOTATION"})
 */
class OptionalProperty extends Optional
{
}
