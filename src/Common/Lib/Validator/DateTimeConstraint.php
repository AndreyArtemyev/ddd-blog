<?php

declare(strict_types=1);

namespace App\Common\Lib\Validator;

use Symfony\Component\Validator\Constraints\DateTime;

/**
 * @Annotation
 */
class DateTimeConstraint extends DateTime
{
    public $format = 'Y-m-d\TH:i:sO';
}
