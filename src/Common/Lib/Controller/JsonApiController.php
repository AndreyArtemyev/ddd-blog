<?php

declare(strict_types=1);

namespace App\Common\Lib\Controller;

use App\Common\Lib\User\CommonUserInterface;
use Ramsey\Uuid\UuidInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;

abstract class JsonApiController extends AbstractController
{
    public function getUserUuid(): UuidInterface
    {
        /** @var CommonUserInterface|null $user */
        $user = $this->getUser();
        if (!$user instanceof CommonUserInterface) {
            throw new UnauthorizedHttpException('User not found');
        }

        return $user->getUuid();
    }

    public function getUserPhone(): string
    {
        /** @var CommonUserInterface|null $user */
        $user = $this->getUser();
        if (!$user instanceof CommonUserInterface) {
            throw new UnauthorizedHttpException('User not found');
        }

        return $user->getUsername();
    }
}
