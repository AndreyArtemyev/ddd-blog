<?php

declare(strict_types=1);

namespace App\Common\Lib\Controller;

abstract class AdminApiController extends JsonApiController
{
    /**
     * @return array
     * @example
     *  [
     *      RoleService::RESOURCE_USER => [
     *          RoleService::PERMISSION_READ,
     *          RoleService::PERMISSION_CREATE
     *      ]
     *  ]
     */
    abstract public static function rolePermissions(): array;
}
