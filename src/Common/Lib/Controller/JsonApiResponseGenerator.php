<?php

declare(strict_types=1);

namespace App\Common\Lib\Controller;

use App\Common\Lib\DebugLogger\DebugLogger;

class JsonApiResponseGenerator
{
    private DebugLogger $debugLogger;

    public function __construct(DebugLogger $debugLogger)
    {
        $this->debugLogger = $debugLogger;
    }

    /**
     * @param mixed|null $data
     * @return JsonApiResponse
     */
    public function success($data = null): JsonApiResponse
    {
        $this->debugLogger->addBreadcrumb(
            self::class . '::form_response',
            ['response' => $data],
            DebugLogger::LEVEL_INFO,
            DebugLogger::CATEGORY_INFRASTRUCTURE,
        );
        return JsonApiResponse::success($data);
    }

    /**
     * @param mixed|null $error
     * @param int $status
     * @return JsonApiResponse
     */
    public function fail($error = null, int $status = 400): JsonApiResponse
    {
        $this->debugLogger->addBreadcrumb(
            self::class . '::form_response',
            ['error' => $error],
            DebugLogger::LEVEL_ERROR,
            DebugLogger::CATEGORY_INFRASTRUCTURE,
        );
        return JsonApiResponse::fail($error);
    }
}
