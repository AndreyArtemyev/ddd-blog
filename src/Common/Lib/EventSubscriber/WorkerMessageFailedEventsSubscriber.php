<?php

declare(strict_types=1);

namespace App\Common\Lib\EventSubscriber;

use App\Common\Lib\DebugLogger\DebugLogger;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Messenger\Event\WorkerMessageFailedEvent;

class WorkerMessageFailedEventsSubscriber implements EventSubscriberInterface
{
    private DebugLogger $debugLogger;

    public function __construct(DebugLogger $debugLogger)
    {
        $this->debugLogger = $debugLogger;
    }

    public function onHandled(WorkerMessageFailedEvent $event): void
    {
        $eventClass = get_class($event->getEnvelope()->getMessage());
        $this->debugLogger->send('event: ' . $eventClass);
        $this->debugLogger->reset();
    }

    public static function getSubscribedEvents(): array
    {
        return [
            WorkerMessageFailedEvent::class => 'onHandled'
        ];
    }
}
