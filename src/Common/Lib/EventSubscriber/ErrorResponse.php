<?php

declare(strict_types=1);

namespace App\Common\Lib\EventSubscriber;

use App\Common\Lib\Controller\JsonApiResponse;
use App\Common\Lib\Dto\Exception\ValidationException;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\HttpException;

class ErrorResponse
{
    protected ContainerInterface $container;
    protected bool $debug;
    protected string $appEnv;

    /**
     * ErrorResponse constructor.
     * @param ContainerInterface $container
     * @param mixed $debug
     * @param mixed $appEnv
     */
    public function __construct(ContainerInterface $container, $debug, $appEnv)
    {
        $this->container = $container;
        $this->debug = (bool)$debug;
        $this->appEnv = (string)$appEnv;
    }

    /**
     * @param ExceptionEvent $event
     * @return void
     */
    public function onKernelException(ExceptionEvent $event): void
    {
        $exception = $event->getThrowable();

        // показать Symfony Exception
        if (
            $this->debug
            && !$exception instanceof \DomainException
            && !$exception instanceof BadRequestHttpException
        ) {
            return;
        }

        $statusCode = 400;
        if ($exception instanceof HttpException) {
            $statusCode = $exception->getStatusCode();
        }

        $message = 'Произошла ошибка. Повторите позже.';
        if ($exception instanceof \DomainException || $exception instanceof HttpException) {
            $message = $exception->getMessage();
        }

        $error = [
            "code" => $exception->getCode(),
            "message" => $message,
            "message_ru" => $message,
        ];
        if ($this->appEnv !== 'prod') {
            $error['trace'] = $exception->getTrace();
        }

        if ($exception instanceof ValidationException) {
            $error['validation'] = $exception->getMessages();
        }

        $event->setResponse(JsonApiResponse::fail($error, $statusCode));
    }
}
