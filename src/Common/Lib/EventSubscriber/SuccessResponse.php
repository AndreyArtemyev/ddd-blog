<?php

declare(strict_types=1);

namespace App\Common\Lib\EventSubscriber;

use App\Common\Lib\Controller\JsonApiResponse;
use App\Common\Lib\DebugLogger\DebugLogger;
use Lexik\Bundle\JWTAuthenticationBundle\Response\JWTAuthenticationFailureResponse;
use Lexik\Bundle\JWTAuthenticationBundle\Response\JWTAuthenticationSuccessResponse;
use Symfony\Component\HttpKernel\Event\ResponseEvent;

class SuccessResponse
{
    private DebugLogger $debugLogger;

    public function __construct(DebugLogger $debugLogger)
    {
        $this->debugLogger = $debugLogger;
    }

    /**
     * @param ResponseEvent $event
     * @throws \JsonException
     */
    public function onKernelResponse(ResponseEvent $event): void
    {
        $statusCode = 200;

        $request = $event->getRequest();
        $this->debugLogger->send($request->getUri());

        $response = $event->getResponse();

        $content = $response->getContent();
        if (!$content) {
            return;
        }

        // Хак для преобразования успешного ответа JWT-либы к общему формату ответа
        if ($response instanceof JWTAuthenticationSuccessResponse) {
            $event->setResponse(
                JsonApiResponse::success(
                    json_decode(
                        $content,
                        true,
                        512,
                        JSON_THROW_ON_ERROR
                    )
                )
            );
        }

        // Хак для преобразовая неудачного ответа JWT-либы к общему формату ответа
        if ($response instanceof JWTAuthenticationFailureResponse) {
            $statusCode = 401;
            $data = json_decode(
                $content,
                true,
                512,
                JSON_THROW_ON_ERROR
            );
            $data['message_ru'] = $data['message'] ?? null;
            $event->setResponse(JsonApiResponse::fail($data, $statusCode));
        }
    }
}
