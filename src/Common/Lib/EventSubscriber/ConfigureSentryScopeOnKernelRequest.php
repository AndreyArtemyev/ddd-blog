<?php

declare(strict_types=1);

namespace App\Common\Lib\EventSubscriber;

use App\Common\Lib\Helpers\CustomHeaders;
use App\Common\Lib\User\CommonUserInterface;
use Ramsey\Uuid\Uuid;
use Sentry\SentrySdk;
use Sentry\State\Scope;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Security\Core\Security;

class ConfigureSentryScopeOnKernelRequest implements EventSubscriberInterface
{
    private Security $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::REQUEST => 'onKernelRequest',
        ];
    }

    public function onKernelRequest(RequestEvent $event): void
    {
        $request = $event->getRequest();
        $requestUuid = $request->headers->get(CustomHeaders::REQUEST_UUID);

        /** @var CommonUserInterface|null $user */
        $user = $this->security->getUser();

        SentrySdk::getCurrentHub()->configureScope(
            function (Scope $scope) use ($user, $requestUuid, $request) {
                if ($user) {
                    $scope->setUser(
                        [
                            'id' => $user->getUuid()->toString(),
                            'username' => $user->getUsername(),
                        ],
                        true
                    );
                }
                $scope->setFingerprint([$this->getUriGroupedByRouteName($request->getUri())]);

                if ($requestUuid) {
                    $scope->setTag('request.uuid', $requestUuid);
                }
            }
        );
    }

    /**
     * Метод обрезает Uuid из параметра урла,для более компактной группировки.
     *
     * @param string $uri
     * @return string
     */
    private function getUriGroupedByRouteName(string $uri)
    {
        $paramUri = substr(strrchr($uri, "/"), 1);
        if (Uuid::isValid($paramUri) && $n = strrpos($uri, "/")) {
            return substr($uri, 0, $n);
        }
        return $uri;
    }
}
