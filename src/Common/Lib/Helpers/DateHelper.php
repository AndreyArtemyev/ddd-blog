<?php

declare(strict_types=1);

namespace App\Common\Lib\Helpers;

class DateHelper
{
    public const FRONT_FORMAT = 'Y-m-d';
    public const DB_FORMAT = 'Y-m-d';

    public static function dateForFrontMust(\DateTimeImmutable $dateTimeImmutable): string
    {
        return $dateTimeImmutable->format(self::FRONT_FORMAT);
    }

    public static function dateForFront(?\DateTimeImmutable $dateTimeImmutable): ?string
    {
        if (!$dateTimeImmutable instanceof \DateTimeImmutable) {
            return null;
        }

        return $dateTimeImmutable->format(self::FRONT_FORMAT);
    }

    public static function dateForFrontFromDBMust(string $dbDate): string
    {
        $dateImmutable = \DateTimeImmutable::createFromFormat(self::DB_FORMAT, $dbDate);
        if (!$dateImmutable instanceof \DateTimeImmutable) {
            throw new \Exception('Check DB scheme');
        }

        return self::dateForFrontMust($dateImmutable);
    }

    public static function dateForFrontFromDB(?string $dbDate): ?string
    {
        if ($dbDate === null) {
            return null;
        }

        return self::dateForFrontFromDBMust($dbDate);
    }

    public static function dateFromFront(string $frontDate): \DateTimeImmutable
    {
        $dateImmutable = \DateTimeImmutable::createFromFormat(self::FRONT_FORMAT, $frontDate);
        if (!$dateImmutable instanceof \DateTimeImmutable) {
            throw new \Exception('Wrong format');
        }

        return $dateImmutable;
    }

    public static function isDate(string $value): bool
    {
        if (!$value) {
            return false;
        }

        try {
            new \DateTimeImmutable($value);
            return true;
        } catch (\Exception $e) {
            return false;
        }
    }
}
