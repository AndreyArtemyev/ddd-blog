<?php

declare(strict_types=1);

namespace App\Common\Lib\Helpers;

class CustomHeaders
{
    public const REQUEST_UUID = 'X-Request-Uuid';
}
