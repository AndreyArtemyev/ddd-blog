<?php

declare(strict_types=1);

namespace App\Common\Lib\Helpers;

use DateTimeZone;

class DateTimeHelper
{
    public const FRONT_FORMAT = 'c';
    public const DB_FORMAT = 'Y-m-d H:i:s';

    public static function forFrontMust(\DateTimeImmutable $dateTimeImmutable): string
    {
        return $dateTimeImmutable->format(self::FRONT_FORMAT);
    }

    public static function forFront(?\DateTimeImmutable $dateTimeImmutable): ?string
    {
        if (!$dateTimeImmutable instanceof \DateTimeImmutable) {
            return null;
        }

        return self::forFrontMust($dateTimeImmutable);
    }

    public static function forFrontFromDBMust(string $dbDateTime): string
    {
        $dateTimeImmutable = \DateTimeImmutable::createFromFormat(self::DB_FORMAT, $dbDateTime);
        if (!$dateTimeImmutable instanceof \DateTimeImmutable) {
            throw new \Exception('Check DB scheme');
        }

        return self::forFrontMust($dateTimeImmutable);
    }

    public static function forFrontFromDB(?string $dbDateTime): ?string
    {
        if ($dbDateTime === null) {
            return null;
        }

        return self::forFrontFromDBMust($dbDateTime);
    }

    public static function forDB(\DateTimeImmutable $dateTimeImmutable): string
    {
        $timeZoneUTC = new DateTimeZone('UTC');
        $dateTimeImmutable = $dateTimeImmutable->setTimezone($timeZoneUTC);

        return $dateTimeImmutable->format(self::DB_FORMAT);
    }

    public static function endOfMinute(\DateTimeImmutable $dateTimeImmutable): \DateTimeImmutable
    {
        return $dateTimeImmutable->setTime(
            (int)$dateTimeImmutable->format('H'),
            (int)$dateTimeImmutable->format('i'),
            59,
            999999
        );
    }
}
