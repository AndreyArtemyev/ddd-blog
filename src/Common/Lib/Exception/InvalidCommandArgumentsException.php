<?php

declare(strict_types=1);

namespace App\Common\Lib\Exception;

class InvalidCommandArgumentsException extends \Exception
{
    protected $message = 'Invalid command argument';
}
