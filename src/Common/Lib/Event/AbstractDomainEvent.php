<?php

declare(strict_types=1);

namespace App\Common\Lib\Event;

use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

abstract class AbstractDomainEvent implements Event
{
    private UuidInterface $uuid;

    private ?UuidInterface $userUuid = null;

    /**
     * @var string
     */
    private string $type;

    /**
     * @var \DateTimeImmutable
     */
    private \DateTimeImmutable $createdAt;

    public function __construct()
    {
        $this->uuid = Uuid::uuid4();

        $this->setType();
        $this->createdAt = new \DateTimeImmutable();
    }

    private function setType(): void
    {
        $path = explode('\\', get_class($this));

        $this->type = array_pop($path);
    }

    /**
     * @param UuidInterface|null $userUuid
     */
    public function setUserUuid(?UuidInterface $userUuid): void
    {
        $this->userUuid = $userUuid;
    }

    public function userUuid(): ?UuidInterface
    {
        return $this->userUuid;
    }

    public function type(): string
    {
        return $this->type;
    }

    public function uuid(): string
    {
        return $this->uuid->toString();
    }

    public function createdAt(): \DateTimeImmutable
    {
        return $this->createdAt;
    }

    abstract public function payload(): array;

    abstract public function oldPayload(): ?array;

    abstract public function entity(): string;

    abstract public function entityUuid(): UuidInterface;
}
