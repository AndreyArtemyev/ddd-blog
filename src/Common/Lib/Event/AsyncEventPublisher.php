<?php

declare(strict_types=1);

namespace App\Common\Lib\Event;

use Symfony\Component\Console\ConsoleEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Messenger\Event\WorkerMessageHandledEvent;
use Throwable;

class AsyncEventPublisher implements EventSubscriberInterface, EventPublisher
{
    /** @var Event[] */
    private array $events = [];

    private MessengerAsyncEventBus $bus;

    /**
     * AsyncEventPublisher constructor.
     * @param MessengerAsyncEventBus $bus
     */
    public function __construct(MessengerAsyncEventBus $bus)
    {
        $this->bus = $bus;
    }

    public function handle(Event $message): void
    {
        $this->events[] = $message;
    }


    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::TERMINATE => 'publish',
            ConsoleEvents::TERMINATE => 'publish',
            WorkerMessageHandledEvent::class => 'publish',
        ];
    }

    /**
     * @throws Throwable
     */
    public function publish(): void
    {
        if (empty($this->events)) {
            return;
        }

        foreach ($this->events as $event) {
            $this->bus->handle($event);
        }

        $this->events = [];
    }
}
