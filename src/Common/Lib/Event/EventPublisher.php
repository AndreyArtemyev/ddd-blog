<?php

declare(strict_types=1);

namespace App\Common\Lib\Event;

use App\Common\Lib\Event\Event;

//TODO так и не понял как он работал раньше, перевел все что оставалось на AsyncEventPublisher
interface EventPublisher
{
    public function handle(Event $message): void;

    public function publish(): void;
}
