<?php

declare(strict_types=1);

namespace App\Common\Lib\Event;

use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

abstract class AbstractInfrastructureEvent implements Event
{
    private UuidInterface $uuid;

    /**
     * @var string
     */
    private string $type;

    /**
     * @var \DateTimeImmutable
     */
    private \DateTimeImmutable $createdAt;

    public function __construct()
    {
        $this->uuid = Uuid::uuid4();

        $this->setType();
        $this->createdAt = new \DateTimeImmutable();
    }

    private function setType(): void
    {
        $path = explode('\\', get_class($this));

        $this->type = array_pop($path);
    }

    public function type(): string
    {
        return $this->type;
    }

    public function uuid(): string
    {
        return $this->uuid->toString();
    }

    public function createdAt(): \DateTimeImmutable
    {
        return $this->createdAt;
    }

    abstract public function payload(): array;
}
