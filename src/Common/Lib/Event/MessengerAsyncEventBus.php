<?php

declare(strict_types=1);

namespace App\Common\Lib\Event;

use Symfony\Component\Messenger\Exception\HandlerFailedException;
use Symfony\Component\Messenger\MessageBusInterface;
use Throwable;

final class MessengerAsyncEventBus
{
    private MessageBusInterface $messageBus;

    public function __construct(MessageBusInterface $messageBus)
    {
        $this->messageBus = $messageBus;
    }

    /**
     * @param Event $command
     * @throws Throwable
     */
    public function handle(Event $command): void
    {
        try {
            $this->messageBus->dispatch($command);
        } catch (HandlerFailedException $error) {
            $this->throwException($error);
        }
    }

    /**
     * @param  Throwable  $exception
     * @throws Throwable
     */
    public function throwException(\Throwable $exception): void
    {
        while ($exception instanceof HandlerFailedException) {
            /** @var Throwable $exception */
            $exception = $exception->getPrevious();
        }

        throw $exception;
    }
}
