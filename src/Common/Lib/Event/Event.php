<?php

declare(strict_types=1);

namespace App\Common\Lib\Event;

interface Event
{
    public function uuid(): string;

    public function type(): string;

    public function createdAt(): \DateTimeImmutable;

    public function payload(): array;
}
