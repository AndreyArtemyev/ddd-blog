<?php

declare(strict_types=1);

namespace App\Common\Lib\DebugLogger;

use Psr\Log\LoggerInterface;
use Sentry\Breadcrumb;
use Sentry\FlushableClientInterface;
use Sentry\SentrySdk;
use Sentry\Severity;
use Sentry\State\HubInterface;

class DebugLogger
{
    public const LEVEL_INFO = Breadcrumb::LEVEL_INFO;
    public const LEVEL_DEBUG = Breadcrumb::LEVEL_DEBUG;
    public const LEVEL_WARNING = Breadcrumb::LEVEL_WARNING;
    public const LEVEL_ERROR = Breadcrumb::LEVEL_ERROR;
    public const LEVEL_FATAL = Breadcrumb::LEVEL_FATAL;

    public const CATEGORY_UI = 'ui';
    public const CATEGORY_APPLICATION = 'application';
    public const CATEGORY_DOMAIN = 'domain';
    public const CATEGORY_INFRASTRUCTURE = 'infrastructure';

    private const ALLOWED_LEVELS = [
        self::LEVEL_INFO,
        self::LEVEL_DEBUG,
        self::LEVEL_WARNING,
        self::LEVEL_ERROR,
        self::LEVEL_FATAL,
    ];

    private const ALLOWED_CATEGORIES = [
        self::CATEGORY_UI,
        self::CATEGORY_APPLICATION,
        self::CATEGORY_DOMAIN,
        self::CATEGORY_INFRASTRUCTURE,
    ];

    private LoggerInterface $logger;

    public function __construct(HubInterface $hub, LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * @param string $message
     * @param array $data
     * @param string $level
     * @param string $category
     */
    public function addBreadcrumb(string $message, array $data, string $level, string $category): void
    {
        if (!in_array($level, self::ALLOWED_LEVELS, true)) {
            return;
        }

        if (!in_array($category, self::ALLOWED_CATEGORIES, true)) {
            return;
        }

        SentrySdk::getCurrentHub()->addBreadcrumb(
            new Breadcrumb(
                $level,
                Breadcrumb::TYPE_DEFAULT,
                $category,
                $message,
                $data,
            )
        );
    }

    /**
     * @param string $message
     * @param array $data
     * @param string $level
     * @param string $category
     */
    public static function addBreadcrumbStatic(string $message, array $data, string $level, string $category): void
    {
        // TODO Возможно эти кейсы надо покрыть эксепшенами
        if (!in_array($level, self::ALLOWED_LEVELS, true)) {
            return;
        }

        if (!in_array($category, self::ALLOWED_CATEGORIES, true)) {
            return;
        }

        SentrySdk::getCurrentHub()->addBreadcrumb(
            new Breadcrumb(
                $level,
                Breadcrumb::TYPE_DEFAULT,
                $category,
                $message,
                $data,
            )
        );
    }

    public function send(string $message): void
    {
        SentrySdk::getCurrentHub()->captureEvent(
            [
                'message' => $message,
                'level' => Severity::info(),
            ]
        );
        $this->logger->info('Data sent to sentry');
    }

    public function reset(): void
    {
        SentrySdk::getCurrentHub()->pushScope()->clearBreadcrumbs();
        $client = SentrySdk::getCurrentHub()->getCLient();
        if ($client instanceof FlushableClientInterface) {
            $this->logger->info('Flush sentry state');
            $client->flush();
        }
    }
}
