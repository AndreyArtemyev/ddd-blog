<?php

declare(strict_types=1);

namespace App\Common\Lib\GlobalDto;

use App\Common\Lib\Dto\DTOCollectionTyped;

/**
 * @template-extends DTOCollectionTyped<int, FilterCommand>
 */
class FilterCommandCollection extends DTOCollectionTyped
{
    public static function getElementClassName(): string
    {
        return FilterCommand::class;
    }
}
