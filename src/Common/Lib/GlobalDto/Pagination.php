<?php

declare(strict_types=1);

namespace App\Common\Lib\GlobalDto;

use App\Common\Lib\Dto\DTO;
use Doctrine\DBAL\ParameterType;
use OpenApi\Annotations as OA;
use Symfony\Component\Validator\Constraints as Assert;

class Pagination extends DTO
{
    /**
     * @OA\Property(property = "byPage", type = "integer",
     *  example = 20,
     * ),
     *
     * @Assert\NotBlank
     * @Assert\Range(min="1")
     */
    public int $byPage;

    /**
     * @OA\Property(property = "page", type = "integer",
     *  example = 1,
     * ),
     *
     * @Assert\NotBlank
     * @Assert\Range(min="1")
     */
    public int $page;

    public function calcOffset(): int
    {
        return ($this->page - 1) * $this->byPage;
    }

    public function generateParamForQuery(): array
    {
        return [
            'offset' => [
                'value' => $this->calcOffset(),
                'type' => ParameterType::INTEGER,
            ],
            'row_count' => [
                'value' => $this->byPage,
                'type' => ParameterType::INTEGER,
            ],
        ];
    }
}
