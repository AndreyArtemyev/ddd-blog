<?php

declare(strict_types=1);

namespace App\Common\Lib\GlobalDto;

use App\Common\Lib\Dto\DTO;
use OpenApi\Annotations as OA;

class PaginationResponse extends DTO
{
    /**
     * @OA\Property(property = "byPage", type = "integer",
     *  example = 20,
     * ),
     */
    public int $byPage;

    /**
     * @OA\Property(property = "page", type = "integer",
     *  example = 1,
     * ),
     */
    public int $page;

    /**
     * @OA\Property(property = "quantity", type = "integer",
     *  example = 123,
     * ),
     */
    public int $quantity;

    public static function create(Pagination $pagination, int $quantity): PaginationResponse
    {
        $paginationResponse = new self();
        $paginationResponse->byPage = $pagination->byPage;
        $paginationResponse->page = $pagination->page;
        $paginationResponse->quantity = $quantity;

        return $paginationResponse;
    }
}
