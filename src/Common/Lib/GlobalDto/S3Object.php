<?php

declare(strict_types=1);

namespace App\Common\Lib\GlobalDto;

use App\Common\Lib\Dto\DTO;

class S3Object extends DTO
{
    public string $body;
    public string $contentType;
}
