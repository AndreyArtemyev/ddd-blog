<?php

declare(strict_types=1);

namespace App\Common\Lib\GlobalDto;

use App\Common\Lib\Dto\DTO;
use OpenApi\Annotations as OA;

class FilterCommand extends DTO
{
    // TODO: @Assert\NotBlank
    // TODO: @Assert\Type("string")
    /**
     * @OA\Property(property = "command", type = "string",
     *  description = "Комманда для фильтрации",
     *  example = "search",
     * )
     */
    public string $command;

    // TODO: @Assert\NotBlank
    /**
     * @OA\Property(property = "data", type = "string",
     *  description = "Необходимы данные для команды, либо значение, либо набор значений, либо объект",
     *  example = "7902",
     * )
     * @var mixed
     */
    public $data;

    public function fetchCommand(): string
    {
        return $this->command;
    }

    /**
     * @return mixed
     */
    public function fetchData()
    {
        return $this->data;
    }
}
