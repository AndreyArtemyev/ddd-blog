<?php

declare(strict_types=1);

namespace App\Common\Lib\GlobalDto;

use App\Common\Lib\Dto\DTO;
use App\Common\Lib\Validator;
use OpenApi\Annotations as OA;
use Symfony\Component\Validator\Constraints as Assert;

class Sorting extends DTO
{
    /**
     * @OA\Property(property = "field", type = "string",
     *  example = "created_at",
     * )
     *
     * @Validator\OptionalProperty({
     *     @Assert\Type("string"),
     * })
     */
    public string $field = '';

    /**
     * @OA\Property(property = "order", type = "string",
     *  example = "asc",
     * )
     *
     * @Validator\OptionalProperty({
     *     @Assert\Type("string"),
     *     @Assert\Choice({"asc", "desc"})
     * })
     */
    public string $order = 'asc';
}
