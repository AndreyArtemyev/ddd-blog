<?php

declare(strict_types=1);

namespace  App\Common\Lib\Doctrine\Type;

use DateTimeImmutable;
use DateTimeZone;
use Doctrine\DBAL\Types\ConversionException;
use Doctrine\DBAL\Types\DateTimeImmutableType;
use Doctrine\DBAL\Platforms\AbstractPlatform;

class CustomDateTimeImmutableType extends DateTimeImmutableType
{
    private const UTC_TIMEZONE = 'UTC';

    /**
     * @param mixed $value
     * @param AbstractPlatform $platform
     * @return string|null
     * @throws ConversionException
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform): ?string
    {
        $converted = $value;
        if ($value instanceof DateTimeImmutable) {
            $converted = $value->setTimezone(new DateTimeZone(self::UTC_TIMEZONE));
        }

        return parent::convertToDatabaseValue($converted, $platform);
    }

    /**
     * @param mixed $value
     * @param AbstractPlatform $platform
     * @return DateTimeImmutable|null
     * @throws ConversionException
     */
    public function convertToPHPValue($value, AbstractPlatform $platform): ?DateTimeImmutable
    {
        if (null === $value || $value instanceof DateTimeImmutable) {
            return $value;
        }

        $converted = DateTimeImmutable::createFromFormat(
            $platform->getDateTimeFormatString(),
            $value,
            new DateTimeZone(self::UTC_TIMEZONE)
        );

        if (! $converted) {
            throw ConversionException::conversionFailedFormat(
                $value,
                $this->getName(),
                $platform->getDateTimeFormatString()
            );
        }

        return $converted;
    }
}
