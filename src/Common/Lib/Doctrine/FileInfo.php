<?php

declare(strict_types=1);

namespace App\Common\Lib\Doctrine;

interface FileInfo
{
    public function getBucket(): string;

    public function getPath(): string;
}
