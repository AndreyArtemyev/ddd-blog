<?php

declare(strict_types=1);

namespace App\Common\Lib\Doctrine;

use DateTimeImmutable;

/**
 * Trait TimestampableImmutableTrait
 * @package App\Common\Domain\Entity
 */
trait TimestampableImmutableTrait
{
    /**
     * @var DateTimeImmutable
     * @Gedmo\Mapping\Annotation\Timestampable(on="create")
     * @Doctrine\ORM\Mapping\Column(type="datetime_immutable")
     */
    private DateTimeImmutable $createdAt;

    /**
     * @var DateTimeImmutable
     * @Gedmo\Mapping\Annotation\Timestampable(on="update")
     * @Doctrine\ORM\Mapping\Column(type="datetime_immutable")
     */
    private DateTimeImmutable $updatedAt;


    public function getCreatedAt(): \DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function getUpdatedAt(): \DateTimeImmutable
    {
        return $this->updatedAt;
    }
}
