<?php

declare(strict_types=1);

namespace App\Common\Lib\Doctrine\Embeddable;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Embeddable()
 */
class Phone
{
    /**
     * @var string
     * @ORM\Column(name="phone", type="string", length=255, nullable=false)
     */
    private string $phone;

    public function __construct(string $phone)
    {
        if (!self::isValid($phone)) {
            throw new \InvalidArgumentException('Invalid phone format');
        }
        $this->phone = $phone;
    }

    public function isEqual(string $phone): bool
    {
        return $phone === $this->phone;
    }

    public function getPhone(): string
    {
        return $this->phone;
    }

    public static function isValid(string $phone): bool
    {
        return (bool)preg_match('|^7\d{10}$|', $phone);
    }

    public static function preparePhoneNumber(string $phone): string
    {
        $phone = preg_replace('![^0-9]+!', '', $phone);
        return $phone;
    }
}
