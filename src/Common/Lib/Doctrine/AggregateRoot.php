<?php

declare(strict_types=1);

namespace App\Common\Lib\Doctrine;

use App\Common\Lib\Event\AbstractDomainEvent;

abstract class AggregateRoot implements \JsonSerializable
{
    protected array $events = [];

    /**
     * @return AbstractDomainEvent[]
     */
    public function popEvents(): array
    {
        $events = $this->events;

        $this->events = [];

        return $events;
    }

    protected function raise(AbstractDomainEvent $event): void
    {
        $this->events[] = $event;
    }

    public function equals(self $entity): bool
    {
        try {
            $result = $this->toArray() == $entity->toArray();
        } catch (\Throwable $e) {
            $result = false;
        }
        return $result;
    }

    abstract public function toArray(): array;
}
