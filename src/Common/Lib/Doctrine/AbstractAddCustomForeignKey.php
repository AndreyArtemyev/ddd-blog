<?php

declare(strict_types=1);

namespace App\Common\Lib\Doctrine;

use Doctrine\ORM\Tools\Event\GenerateSchemaEventArgs;

abstract class AbstractAddCustomForeignKey
{
    protected const TABLES = [];

    public function postGenerateSchema(GenerateSchemaEventArgs $args): void
    {
        $schema = $args->getSchema();
        $em = $args->getEntityManager();

        foreach ($schema->getTables() as $table) {
            if (!isset(static::TABLES[$table->getName()])) {
                continue;
            }

            foreach (static::TABLES[$table->getName()]['indexes'] as $index) {
                $table->addIndex(
                    $index['columnNames'],
                    $index['indexName']
                );
            }

            foreach (static::TABLES[$table->getName()]['foreignKeys'] as $foreignKey) {
                $table->addForeignKeyConstraint(
                    $foreignKey['foreignTable'],
                    $foreignKey['localColumnNames'],
                    $foreignKey['foreignColumnNames'],
                    $foreignKey['options'],
                    $foreignKey['constraintName'],
                );
            }
        }
    }
}
