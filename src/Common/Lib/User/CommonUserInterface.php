<?php

declare(strict_types=1);

namespace App\Common\Lib\User;

use Ramsey\Uuid\UuidInterface;
use Symfony\Component\Security\Core\User\UserInterface as BaseUserInterface;

interface CommonUserInterface extends BaseUserInterface
{
    public function getUuid(): UuidInterface;
}
