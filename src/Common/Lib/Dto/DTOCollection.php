<?php

declare(strict_types=1);

namespace App\Common\Lib\Dto;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * @psalm-template TKey of array-key
 * @psalm-template T of DTO
 * @template-extends ArrayCollection<TKey, T>
 */
class DTOCollection extends ArrayCollection implements \JsonSerializable
{
    public function jsonSerialize()
    {
        return $this->toArray();
    }
}
