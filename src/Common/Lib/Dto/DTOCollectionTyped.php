<?php

declare(strict_types=1);

namespace App\Common\Lib\Dto;

/**
 * @psalm-template TKey of array-key
 * @psalm-template T of DTO
 * @template-extends DTOCollection<TKey, T>
 */
abstract class DTOCollectionTyped extends DTOCollection
{
    /**
     * @psalm-return class-string<T>
     */
    abstract public static function getElementClassName(): string;
}
