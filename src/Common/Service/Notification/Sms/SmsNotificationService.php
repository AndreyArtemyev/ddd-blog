<?php

declare(strict_types=1);

namespace App\Common\Service\Notification\Sms;

use App\Common\Service\Notification\IstelecomClient;
use App\Common\Service\Notification\NexmoClient;
use App\Common\Service\Notification\SmsRuClient;

class SmsNotificationService
{
    public const RUSSIAN_PHONE_CODE = "RU";
    public const KAZAKH_PHONE_CODE = "KZ";

    /** @var NexmoClient */
    private NexmoClient $nexmoClient;

    /** @var IstelecomClient */
    private IstelecomClient $istelecomClient;

    /** @var SmsRuClient */
    private SmsRuClient $smsRuClient;

    public function __construct(NexmoClient $nexmoClient, IstelecomClient $istelecomClient, SmsRuClient $smsRuClient)
    {
        $this->nexmoClient = $nexmoClient;
        $this->istelecomClient = $istelecomClient;
        $this->smsRuClient = $smsRuClient;
    }

    /**
     * @param string $receiverPhone
     * @param string $message
     * @param string $countryCode
     * @return bool
     * @throws \Exception
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function send(string $receiverPhone, string $message, string $countryCode): bool
    {
//        switch ($countryCode) {
//            case self::RUSSIAN_PHONE_CODE:
//                return $this->istelecomClient->sendRuSms($receiverPhone, $message);
//            case self::KAZAKH_PHONE_CODE:
//                return $this->istelecomClient->sendKzSms($receiverPhone, $message);
//            default:
//                return $this->nexmoClient->sendSms($receiverPhone, $message);
//        }
        return $this->smsRuClient->sendSms($receiverPhone, $message);
    }
}
