<?php

declare(strict_types=1);

namespace App\Common\Service\Notification\Websocket;

use phpcent\Client;

class CentrifugoClient
{
    private Client $client;

    public function __construct()
    {
        $this->client = new Client(
            $_ENV['CENTRIFUGO_URL'] ?? 'centrifugo:8000/api',
            $_ENV['CENTRIFUGO_API_KEY'] ?? 'my_api_key',
        );
        $this->client->setUseAssoc(true);
    }

    /**
     * Публикация данных для одного канала
     *
     * @param string $channel
     * @param array $data
     */
    public function publish(string $channel, array $data): void
    {
        $this->client->publish(
            $this->addChannelNamePrefix($channel),
            $data
        );
    }

    /**
     * Публикация данных для нескольких каналов
     *
     * @param array $channels
     * @param array $data
     */
    public function broadcast(array $channels, array $data): void
    {
        $channelsWithPrefix = array_map(
            function ($channel) {
                return $this->addChannelNamePrefix($channel);
            },
            $channels
        );

        $this->client->broadcast(
            $channelsWithPrefix,
            $data
        );
    }

    public function getHistory(string $channel): array
    {
        $response = $this->client->history($this->addChannelNamePrefix($channel));
        if (empty($response['result']['publications'])) {
            return [];
        }
        $result = [];
        foreach ($response['result']['publications'] as $publication) {
            $result[] = $publication['data'];
        }

        return $result;
    }

    /**
     * Добавление префикса для наименования канала в зависимости от текущего урла бэкенда
     *
     * @param string $channelName
     * @return string
     */
    private function addChannelNamePrefix(string $channelName): string
    {
        $channelPrefix = isset($_ENV['URL_DOMAIN']) ? $_ENV['URL_DOMAIN'] . '.' : '';
        return $channelPrefix . $channelName;
    }
}
