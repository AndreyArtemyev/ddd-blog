<?php

declare(strict_types=1);

namespace App\Common\Service\Notification\Websocket;

class CentrifugoChannelNameBase extends CentrifugoChannelNameAbstract
{
    // Список все каналов веб-сокетов
    public const APP_INFO = 'app-info';
}
