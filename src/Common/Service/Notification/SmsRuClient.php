<?php

declare(strict_types=1);

namespace App\Common\Service\Notification;

use IM\Bundle\SharedKernelBundle\Exception\InfrastructureException;
use GuzzleHttp\Client;

/**
 * Class SmsRuClient
 * @package IM\Bundle\SharedKernelBundle\Service\Notification
 */
class SmsRuClient
{
    public const RESPONSE_OK = 100;

    /**
     * @var Client
     */
    private Client $httpClient;

    /**
     * SmsRuClient constructor.
     */
    public function __construct()
    {
        $this->httpClient = new Client();
    }

    /**
     * @param string $phone
     * @param string $message
     * @return bool
     * @throws InfrastructureException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function sendSms(string $phone, string $message): bool
    {
        $url        = $_ENV['SMSRU_URL'] ?? null;
        $senderName = $_ENV['SMSRU_SENDER_NAME'] ?? null;
        $apiKey     = $_ENV['SMSRU_API_KEY'] ?? null;

        if (!$url || !$senderName || !$apiKey) {
            throw new InfrastructureException('SMS.RU service credentials is missing');
        }

        $query = [
            'api_id' => $apiKey,
            'to'     => $phone,
            'msg'    => $message,
            'from'   => $senderName,
            'json'   => 1
        ];

        $response = $this->httpClient->get($url, [
            'query' => $query
        ]);

        $responseContent = json_decode($response->getBody()->getContents());

        if (
            ($response->getStatusCode() !== 200)
            || !$responseContent->status_code
            || ($responseContent->status_code !== self::RESPONSE_OK)
        ) {
            throw new InfrastructureException('Sms is not sent');
        }

        return true;
    }
}
