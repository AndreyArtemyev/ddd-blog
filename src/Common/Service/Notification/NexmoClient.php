<?php

declare(strict_types=1);

namespace App\Common\Service\Notification;

use App\Common\Lib\Exception\InfrastructureException;
use GuzzleHttp\Client;

class NexmoClient
{
    private $httpClient;

    public function __construct()
    {
        $this->httpClient = new Client();
    }

    /**
     * @param string $phone
     * @param string $message
     * @return bool
     * @throws InfrastructureException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function sendSms(string $phone, string $message): bool
    {
        $apiUrl = $_ENV['NEXMO_API_URL'] ?? null;
        $apiKey = $_ENV['NEXMO_API_KEY'] ?? null;
        $apiSecret = $_ENV['NEXMO_API_SECRET'] ?? null;

        if (!$apiUrl || !$apiKey || !$apiSecret) {
            throw new InfrastructureException('SMS service credentials is missing');
        }

        $params = [
            'api_key' => $apiKey,
            'to' => $phone,
            'text' => $message,
            'from' => 'EVSERVER', // TODO Возможно надо поменять?
            'api_secret' => $apiSecret,
        ];

        $response = $this->httpClient->post($apiUrl, [
            'form_params' => $params
        ]);

        // TODO 200 Заменить на константу
        if ($response->getStatusCode() === 200) {
            return true;
        }

        throw new InfrastructureException('Sms is not sent');
    }
}
