<?php

declare(strict_types=1);

namespace App\Common\Service\Notification;

use IM\Bundle\SharedKernelBundle\Exception\InfrastructureException;
use GuzzleHttp\Client;

class IstelecomClient
{
    private const SENDER_NAME_VIBER = "BRIXBY",
        SENDER_NAME_SMS = "i-mobility",
        SERVICE_SMS = "sms",
        SERVICE_INTERMOBILITY_SMS = "tgw-sms",
        SERVICE_VIBER = "viber",
        SENDER_NAME_SMS_KCELL = "NSParking";

    private $httpClient;

    public function __construct()
    {
        $this->httpClient = new Client();
    }

    /**
     * @param string $receiver
     * @param string $message
     * @throws \Exception
     */
    public function sendRuSms(string $receiver, string $message): bool
    {
        $params = [
            'sender' => self::SENDER_NAME_SMS,
            'receiver' => '+' . $receiver,
            'text' => $message,
            'service' => self::SERVICE_INTERMOBILITY_SMS,
        ];

        return $this->send($params);
    }

    /**
     * @param string $receiver
     * @param string $message
     * @throws \Exception
     */
    public function sendKzSms($receiver, $message): bool
    {
        $params = [
            'sender' => self::SENDER_NAME_SMS_KCELL,
            'receiver' => '+' . $receiver,
            'text' => $message,
            'service' => self::SERVICE_SMS,
        ];

        return $this->send($params);
    }

    /**
     * @param array $params
     * @return bool
     * @throws InfrastructureException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \JsonException
     */
    private function send(array $params): bool
    {
        $authCode = $this->auth();

        $response = $this->httpClient->post(
            "https://mdp1.istelecom.msk.ru/api/v1/messages/",
            [
                'body' => json_encode($params, JSON_THROW_ON_ERROR),
                'headers' => [
                    'Authorization' => $authCode
                ]
            ]
        );

        // TODO 200 Заменить на константу
        if ($response->getStatusCode() === 200) { // Получен ответ от сервера
            return true;
        }
        throw new InfrastructureException('Sms is not sent');
    }

    /**
     *
     * @return string|null
     * @psalm-suppress all
     */
    private function auth()
    {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, "https://mdp1.istelecom.msk.ru/api/v1/login/access-token");

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            "Content-Type" => "application/x-www-form-urlencoded"
        ]);
        curl_setopt($ch, CURLOPT_POSTFIELDS, [
            'username' => $_ENV['ISTELECOM_USERNAME'] ?? null,
            'password' => $_ENV['ISTELECOM_PASSWORD'] ?? null,
        ]);

        $response = \GuzzleHttp\json_decode(curl_exec($ch));

        curl_close($ch);

        return isset($response->access_token) ? "Bearer " . $response->access_token : null;
    }
}
