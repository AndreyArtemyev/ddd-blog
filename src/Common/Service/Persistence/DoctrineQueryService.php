<?php

declare(strict_types=1);

namespace App\Common\Service\Persistence;

use App\Common\Lib\Dto\DTOCollection;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Result;
use Doctrine\DBAL\ParameterType;
use Doctrine\ORM\EntityManagerInterface;

final class DoctrineQueryService
{
    private Connection $connection;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->connection = $entityManager->getConnection();
    }

    private function statementExec(string $sqlQuery, array $bindValues): Result
    {
        $statement = $this->connection->prepare($sqlQuery);

        if (!empty($bindValues)) {
            foreach ($bindValues as $bindKey => $bindValue) {
                $bindValueType = ParameterType::STRING;
                if (is_array($bindValue)) {
                    ['value' => $bindValue, 'type' => $bindValueType] = $bindValue;
                }
                $statement->bindValue($bindKey, $bindValue, $bindValueType);
            }
        }

        return $statement->executeQuery();
    }

    /**
     * @template T of DTO
     * @param string $sqlQuery
     * @param array $bindValues
     * @param class-string<T> $class
     * @return DTOCollection<int, T>
     */
    public function fetchAllDTO(
        string $sqlQuery,
        array $bindValues,
        string $class
    ): DTOCollection {
        $queryResult = $this->statementExec($sqlQuery, $bindValues);

        /** @psalm-var DTOCollection<int, T> $result */
        $result = new DTOCollection();
        foreach ($queryResult->fetchAllAssociative() as $row) {
            $result->add(new $class($row));
        }
        $queryResult->free();

        return $result;
    }

    /**
     * @template T
     * @param string $sqlQuery
     * @param array $bindValues
     * @param class-string<T> $class
     * @return T|null
     */
    public function fetchDTO(
        string $sqlQuery,
        array $bindValues,
        string $class
    ) {
        $queryResult = $this->statementExec($sqlQuery, $bindValues);

        $row = $queryResult->fetchAssociative();
        $queryResult->free();

        if ($row === false) {
            return null;
        }
        return new $class($row);
    }

    /**
     * @param string $sqlQuery
     * @param array $bindValues
     * @return int
     */
    public function fetchInt(
        string $sqlQuery,
        array $bindValues
    ): int {
        $queryResult = $this->statementExec($sqlQuery, $bindValues);

        $result = (int)$queryResult->fetchOne();
        $queryResult->free();
        return $result;
    }

    /**
     * Метод преобразует массив в строку, для использования в запросе.
     *
     * @param array $array
     * @return string
     */
    public function arrayToString(array $array): string
    {
        $addQuotes = function ($str) {
            if (is_int($str)) {
                return $str;
            } else {
                return $this->connection->quote($str);
            }
        };
        return implode(',', array_map($addQuotes, $array));
    }


    /**
     * TODO убрать этот метод, этот класс должен возвращать либо объекты, либо базовые типы
     * @param string $sqlQuery
     * @param array $bindValues
     * @return array
     */
    public function fetchAllAssociative(
        string $sqlQuery,
        array $bindValues = []
    ): array {
        $queryResult = $this->statementExec($sqlQuery, $bindValues);

        $result = $queryResult->fetchAllAssociative();
        $queryResult->free();
        return $result;
    }
}
