<?php

declare(strict_types=1);

namespace App\Common\Service\Persistence;

use App\Common\Lib\Doctrine\AggregateRoot;
use App\Common\Lib\Event\AsyncEventPublisher;
use App\Common\Lib\User\CommonUserInterface;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ObjectRepository;
use Symfony\Component\Security\Core\Security;

final class DoctrineRepository
{
    protected EntityManagerInterface $em;

    protected AsyncEventPublisher $eventPublisher;
    protected Security $security;

    public function __construct(
        EntityManagerInterface $entityManager,
        AsyncEventPublisher $eventPublisher,
        Security $security
    ) {
        $this->em = $entityManager;
        $this->eventPublisher = $eventPublisher;

        $this->security = $security;
    }

    /**
     * @param string $className
     * @psalm-param class-string $className
     * @psalm-return ObjectRepository
     */
    public function getObjectRepository(string $className): ObjectRepository
    {
        return $this->em->getRepository($className);
    }

    public function createQuery(string $sql): Query
    {
        return $this->em->createQuery($sql);
    }

    public function getQueryBuilder(): QueryBuilder
    {
        return $this->em->createQueryBuilder();
    }

    /**
     * @param mixed $entity
     */
    public function persist($entity): void
    {
        $this->em->persist($entity);
    }

    public function flush(): void
    {
        $this->em->flush();
    }

    /**
     * @param object ...$entities
     */
    public function save(object ...$entities): void
    {
        $this->checkAndResetEntityManager();

        foreach ($entities as $entity) {
            $this->em->persist($entity);
        }
        $this->em->flush();
        $this->em->clear();

        foreach ($entities as $entity) {
            if ($entity instanceof AggregateRoot) {
                $this->releaseEvents($entity);
            }
        }
    }

    /**
     * @param object ...$entities
     * @throws \Throwable
     */
    public function transactionalSave(object ...$entities): void
    {
        $this->checkAndResetEntityManager();

        $this->em->beginTransaction();
        try {
            foreach ($entities as $entity) {
                $this->em->persist($entity);
                $this->em->flush();
            }

            $this->em->commit();
            $this->em->clear();

            foreach ($entities as $entity) {
                if ($entity instanceof AggregateRoot) {
                    $this->releaseEvents($entity);
                }
            }
        } catch (\Throwable $throwable) {
            $this->em->rollback();
            throw $throwable;
        }
    }

    /**
     * @param object ...$entities
     */
    public function delete(object ...$entities): void
    {
        foreach ($entities as $entity) {
            $this->em->remove($entity);
        }
        $this->em->flush();

        foreach ($entities as $entity) {
            if ($entity instanceof AggregateRoot) {
                $this->releaseEvents($entity);
            }
        }
    }

    private function releaseEvents(AggregateRoot $entity): void
    {
        foreach ($entity->popEvents() as $event) {
            /** @var CommonUserInterface|null $user */
            $user = $this->security->getUser();
            if ($user) {
                $event->setUserUuid($user->getUuid());
            }
            $this->eventPublisher->handle($event);
        }
    }

    private function checkAndResetEntityManager(): void
    {
        if (!$this->em->isOpen()) {
            $this->em = EntityManager::create(
                $this->em->getConnection(),
                $this->em->getConfiguration(),
                $this->em->getEventManager()
            );
        }
    }
}
