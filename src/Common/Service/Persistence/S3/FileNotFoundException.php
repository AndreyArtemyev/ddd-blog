<?php

declare(strict_types=1);

namespace App\Common\Service\Persistence\S3;

class FileNotFoundException extends \Exception
{
    protected $message = 'File not found';
}
