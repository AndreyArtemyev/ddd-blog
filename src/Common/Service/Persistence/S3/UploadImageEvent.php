<?php

declare(strict_types=1);

namespace IM\Bundle\SharedKernelBundle\Service\Persistence\S3;

use IM\Bundle\SharedKernelBundle\Lib\Event\AbstractInfrastructureEvent;

class UploadImageEvent extends AbstractInfrastructureEvent
{
    private string $key;
    private string $currentBucket;
    private string $newBucket;

    public function __construct(string $key, string $currentBucket, string $newBucket)
    {
        parent::__construct();
        $this->currentBucket = $currentBucket;
        $this->newBucket = $newBucket;
        $this->key = $key;
    }

    /**
     * @return string
     */
    public function getKey(): string
    {
        return $this->key;
    }

    /**
     * @return string
     */
    public function getCurrentBucket(): string
    {
        return $this->currentBucket;
    }

    /**
     * @return string
     */
    public function getNewBucket(): string
    {
        return $this->newBucket;
    }

    public function payload(): array
    {
        return [
            'key'           => $this->key,
            'currentBucket' => $this->currentBucket,
            'newBucket'     => $this->newBucket
        ];
    }
}
