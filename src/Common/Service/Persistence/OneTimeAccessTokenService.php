<?php

declare(strict_types=1);

namespace App\Common\Service\Persistence;

use App\Common\Lib\Exception\InfrastructureException;
use Ramsey\Uuid\Uuid;

class OneTimeAccessTokenService
{
    private RedisRepository $redisRepository;

    public function __construct(RedisRepository $redisRepository)
    {
        $this->redisRepository = $redisRepository;
    }

    /**
     * @param mixed $value
     * @return string
     * @throws \JsonException
     */
    public function create($value): string
    {
        $token = Uuid::uuid4()->toString();
        $this->redisRepository->set($token, $value, RedisRepository::FIVE_MINUTE_TIMEOUT);
        return $token;
    }

    /**
     * @param string $token
     * @return mixed
     * @throws InfrastructureException
     * @throws \JsonException
     */
    public function get(string $token)
    {
        $value = $this->redisRepository->GETDEL($token);
        if ($value === null) {
            throw new InfrastructureException('One time token not found or expired');
        }

        return $value;
    }
}
