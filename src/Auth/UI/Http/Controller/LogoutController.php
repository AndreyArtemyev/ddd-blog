<?php

declare(strict_types=1);

namespace App\Auth\UI\Http\Controller;

use App\Auth\Domain\Entity\Session;
use App\Auth\Domain\Exception\ActiveSessionNotFoundException;
use App\Auth\Domain\Service\SessionRepository;
use App\Common\Lib\Controller\JsonApiController;
use App\Common\Lib\Controller\JsonApiResponse;
use Lexik\Bundle\JWTAuthenticationBundle\Exception\InvalidTokenException;
use Nelmio\ApiDocBundle\Annotation\Operation;
use OpenApi\Annotations as OA;
use Ramsey\Uuid\Uuid;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class LogoutController extends JsonApiController
{
    private SessionRepository $sessionRepository;

    public function __construct(SessionRepository $sessionRepository)
    {
        $this->sessionRepository = $sessionRepository;
    }

    /**
     * @Route("/logout", name = "auth_logout", methods = {"POST"})
     *
     * @Operation(
     *  tags = {"Auth"},
     *  summary = "Logout",
     *  @OA\Response(
     *      response = "200",
     *      description = "Returned when successful",
     *      @OA\JsonContent(
     *          type="object",
     *          @OA\Property(property = "success", type = "boolean",
     *              example = true
     *          ),
     *          @OA\Property(property = "error", type = "object", nullable = true,
     *              example = null
     *          ),
     *          @OA\Property(property = "data", type = "object", nullable = true,
     *              example = null,
     *          ),
     *      ),
     *  ),
     * )
     *
     * @param Request $request
     * @return JsonApiResponse
     */
    public function __invoke(Request $request): JsonApiResponse
    {
        $sessionUuid = $request->request->get('sessionUuid');

        if (empty($sessionUuid) || !is_string($sessionUuid)) {
            throw new InvalidTokenException();
        }

        $sessionUuid = Uuid::fromString($sessionUuid);
        $activeSession = $this->sessionRepository->findOneActiveSession($sessionUuid);

        if (!($activeSession instanceof Session)) {
            throw new ActiveSessionNotFoundException('', 'Ваш текущий сеанс истёк');
        }

        $activeSession->finish();
        $this->sessionRepository->save($activeSession);

        return JsonApiResponse::success();
    }
}
