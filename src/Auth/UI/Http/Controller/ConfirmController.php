<?php

declare(strict_types=1);

namespace App\Auth\UI\Http\Controller;

use App\Auth\Domain\Service\AuthService;
use App\Auth\Domain\Service\SessionRepository;
use App\Common\Lib\DebugLogger\DebugLogger;
use Lexik\Bundle\JWTAuthenticationBundle\Security\Http\Authentication\AuthenticationSuccessHandler;
use Nelmio\ApiDocBundle\Annotation\Operation;
use OpenApi\Annotations as OA;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Routing\Annotation\Route;

class ConfirmController
{
    private AuthenticationSuccessHandler $authSuccess;
    private AuthService $authService;
    private SessionRepository $sessionRepository;
    private DebugLogger $debugLogger;

    public function __construct(
        AuthenticationSuccessHandler $authSuccess,
        AuthService $authService,
        SessionRepository $sessionRepository,
        DebugLogger $debugLogger
    ) {
        $this->authSuccess = $authSuccess;
        $this->authService = $authService;
        $this->sessionRepository = $sessionRepository;
        $this->debugLogger = $debugLogger;
    }

    /**
     * @Route("/confirm", name = "auth_confirm", methods = {"POST"})
     *
     * @Operation(
     *  tags = {"Auth"},
     *  summary = "Подтверждение пин-кода",
     *  @OA\RequestBody(
     *      @OA\JsonContent(
     *          type = "object",
     *          required = {"phone", "country_code", "code"},
     *          @OA\Property(property = "phone", type = "string",
     *              description = "Телефон для регистрации",
     *              example = "9789999999",
     *          ),
     *          @OA\Property(property = "country_code", type = "string",
     *              description = "Код страны http://country.io/names.json",
     *              example = "RU",
     *          ),
     *          @OA\Property(property = "code", type = "integer",
     *              description = "Код подтверждения",
     *              example = 5555,
     *          ),
     *      )
     *  ),
     *  @OA\Response(
     *      response = "200",
     *      description = "Returned when successful",
     *      @OA\JsonContent(
     *          type="object",
     *          @OA\Property(property = "success", type = "boolean",
     *              example = true
     *          ),
     *          @OA\Property(property = "error", type = "object", nullable = true,
     *              example = null
     *          ),
     *          @OA\Property(property = "data", type = "object",
     *              @OA\Property(property = "token", type = "string"),
     *              @OA\Property(property = "refresh_token", type = "string"),
     *          ),
     *      ),
     *  ),
     * )
     *
     * @param Request $request
     * @throws \App\Auth\Domain\Exception\InvalidPhoneNumberException
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \App\Auth\Domain\Exception\InvalidConfirmationCodeException
     *@return \Lexik\Bundle\JWTAuthenticationBundle\Response\JWTAuthenticationSuccessResponse
     */
    public function confirmUser(Request $request): Response
    {
        $countryCode = $request->request->get('country_code');
        $phone = (string)$request->request->get('phone');
        $code = (int)$request->request->get('code');

        $clientType = $request->headers->get('x-client-type');
        $clientVersion = $request->headers->get('x-client-version');
        $clientBuild = $request->headers->get('x-client-build');

        $this->debugLogger->addBreadcrumb(
            self::class . '::form_variables',
            [
                'phone'       => $phone,
                'countryCode' => $countryCode,
                'code'        => $code,
            ],
            DebugLogger::LEVEL_INFO,
            DebugLogger::CATEGORY_UI,
        );

        if (!is_string($countryCode) || empty($phone) || empty($code)) {
            throw new BadRequestHttpException();
        }

        $user = $this->authService->confirmUser($countryCode, $phone, $code);
        $session = $this->sessionRepository->createSession($user, $clientType, $clientVersion, $clientBuild);
        $request->request->set('sessionUuid', $session->getUuid()->toString());

        return $this->authSuccess->handleAuthenticationSuccess($user);
    }
}
