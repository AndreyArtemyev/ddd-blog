<?php

declare(strict_types=1);

namespace App\Auth\UI\Http\Controller;

use App\Auth\Domain\Service\AuthService;
use App\Common\Lib\Controller\JsonApiResponse;
use App\Common\Lib\DebugLogger\DebugLogger;
use Nelmio\ApiDocBundle\Annotation\Operation;
use OpenApi\Annotations as OA;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Routing\Annotation\Route;

class SendCodeController
{
    private AuthService $authService;
    private DebugLogger $debugLogger;

    public function __construct(AuthService $authService, DebugLogger $debugLogger)
    {
        $this->authService = $authService;
        $this->debugLogger = $debugLogger;
    }

    /**
     * @Route("/send-code", name = "auth_send_code", methods = {"POST"})
     *
     * @Operation(
     *  tags = {"Auth"},
     *  summary = "Регистрация/Получение нового pin кода",
     *  @OA\RequestBody(
     *      @OA\JsonContent(
     *          type = "object",
     *          required = {"phone", "country_code"},
     *          @OA\Property(property = "phone", type = "string",
     *              description = "Телефон для регистрации",
     *              example = "9789999999",
     *          ),
     *          @OA\Property(property = "country_code", type = "string",
     *              description = "Код страны http://country.io/names.json",
     *              example = "RU",
     *          ),
     *      )
     *  ),
     *  @OA\Response(
     *      response = "200",
     *      description = "Returned when successful",
     *      @OA\JsonContent(
     *          type = "object",
     *          @OA\Property(property = "success", type = "boolean",
     *              example = true,
     *          ),
     *          @OA\Property(property = "error", type = "object", nullable = true,
     *              example = null,
     *          ),
     *          @OA\Property(property = "data", type = "object", nullable = true,
     *              example = null,
     *          ),
     *      ),
     *  ),
     * )
     *
     * @param Request $request
     * @throws \App\Auth\Domain\Exception\InvalidPhoneNumberException
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \GuzzleHttp\Exception\GuzzleException
     *@return Response
     */
    public function sendCode(Request $request): Response
    {
        $countryCode = $request->request->get('country_code');
        $phone = $request->request->get('phone');

        $this->debugLogger->addBreadcrumb(
            self::class . '::form_variables',
            ['phone' => $phone, 'countryCode' => $countryCode],
            DebugLogger::LEVEL_INFO,
            DebugLogger::CATEGORY_UI,
        );

        if (!is_string($countryCode) || !is_string($phone)) {
            throw new BadRequestHttpException('Invalid country_code or phone');
        }

        $codeIsSent = $this->authService->sendCode($countryCode, $phone);
        if (!$codeIsSent) {
            throw new BadRequestHttpException('Code is not sent');
        }

        return JsonApiResponse::success();
    }
}
