<?php

declare(strict_types=1);

namespace App\Auth\UI\Http\Controller;

use App\Auth\Application\DTO\RefreshToken;
use App\Auth\Domain\Entity\Session;
use App\Auth\Domain\Exception\ActiveSessionNotFoundException;
use App\Auth\Domain\Service\SessionRepository;
use App\Auth\Domain\Service\UserRepositoryService;
use App\Common\Lib\Dto\DTOFactory;
use Lexik\Bundle\JWTAuthenticationBundle\Security\Http\Authentication\AuthenticationSuccessHandler;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Operation;
use OpenApi\Annotations as OA;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class RefreshTokenController
{
    private AuthenticationSuccessHandler $authSuccess;
    private SessionRepository $sessionRepository;
    private UserRepositoryService $userRepository;

    public function __construct(
        AuthenticationSuccessHandler $authSuccess,
        SessionRepository $sessionRepository,
        UserRepositoryService $userRepository
    ) {
        $this->authSuccess = $authSuccess;
        $this->sessionRepository = $sessionRepository;
        $this->userRepository = $userRepository;
    }

    /**
     * @Route("/refresh-token", name = "refresh_token", methods = {"POST"})
     *
     * @Operation(
     *  tags = {"Refresh token"},
     *  summary = "Refresh token",
     *  @OA\RequestBody(
     *      @OA\JsonContent(
     *          type = "object",
     *          @OA\Property(property = "refresh_token",
     *              ref = @Model(type = RefreshToken::class)
     *          ),
     *      )
     *  ),
     *  @OA\Response(
     *      response = "200",
     *      description = "Returned when successful",
     *      @OA\JsonContent(
     *          type="object",
     *          @OA\Property(property = "success", type = "boolean",
     *              example = true
     *          ),
     *          @OA\Property(property = "error", type = "object", nullable = true,
     *              example = null
     *          ),
     *          @OA\Property(property = "data", type = "object",
     *              @OA\Property(property = "token", type = "string"),
     *              @OA\Property(property = "refresh_token", type = "string"),
     *          ),
     *      ),
     *  ),
     * )
     *
     * @param Request $request
     * @return Response
     */
    public function __invoke(Request $request): Response
    {
        $refreshToken = DTOFactory::createDtoFromRequest(RefreshToken::class, $request);

        $activeSession = $this->sessionRepository->findOneActiveSessionByRefreshToken($refreshToken->refresh_token);

        if (!($activeSession instanceof Session)) {
            throw new ActiveSessionNotFoundException('', 'Ваш текущий сеанс истёк');
        }

        $activeSession->setRefreshToken(null);
        $this->sessionRepository->save($activeSession);

        $request->request->set('sessionUuid', $activeSession->getUuid()->toString());
        $user = $this->userRepository->findOneOrFail($activeSession->getUserUuid());

        return $this->authSuccess->handleAuthenticationSuccess($user);
    }
}
