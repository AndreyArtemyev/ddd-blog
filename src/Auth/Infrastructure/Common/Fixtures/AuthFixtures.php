<?php

namespace App\Auth\Infrastructure\Common\Fixtures;

use App\Auth\Domain\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AuthFixtures extends Fixture
{
    public const AUTH_USER_REFERENCE = 'auth_users_';

    public function load(ObjectManager $manager): void
    {
        for ($i = 1; $i < 10; $i++) {
            $userName = '7978999999' . $i;
            $userPass = $this->hashPassword((string)$i);
            $user = User::create($userName, $userPass);
            $manager->persist($user);
            $this->addReference(self::AUTH_USER_REFERENCE . $i, $user);
        }
        $manager->flush();
    }

    /**
     * @param $password
     * @return string
     */
    private function hashPassword(string $password): string
    {
        /** @var string|bool|null $hashedPassword */
        $hashedPassword = \password_hash($password, PASSWORD_BCRYPT, ['cost' => 12]);

        if (\is_bool($hashedPassword)) {
            throw new \RuntimeException('Server error hashing password');
        }

        return (string)$hashedPassword;
    }
}
