<?php

declare(strict_types=1);

namespace App\Auth\Application\DTO;

use App\Common\Lib\Dto\DTO;
use App\Common\Lib\Helpers\DateTimeHelper;
use OpenApi\Annotations as OA;

class AdminUserForList extends DTO
{
    /**
     * @OA\Property(property = "uuid", type = "string",
     *  example = "2ef0507c-a1c3-4d9d-81fa-794dd31a1290",
     * ),
     */
    public string $uuid;

    /**
     * @OA\Property(property = "username", type = "string",
     *  example = "79789999999",
     * ),
     */
    public string $username;

    /**
     * @OA\Property(property = "phone_confirmed_at", type = "string", nullable=true,
     *  example = "2021-05-19T00:04:03+00:00",
     * ),
     */
    public ?string $phone_confirmed_at;

    /**
     * @return array
     */
    public function jsonSerialize(): array
    {
        $result = parent::jsonSerialize();
        $result['phone_confirmed_at'] = DateTimeHelper::forFrontFromDB($this->phone_confirmed_at);

        return $result;
    }
}
