<?php

declare(strict_types=1);

namespace App\Auth\Application\DTO;

use App\Common\Lib\Dto\DTO;
use OpenApi\Annotations as OA;
use Symfony\Component\Validator\Constraints as Assert;

class RefreshToken extends DTO
{
    /**
     * @OA\Property(property = "refresh_token", type = "string",
     *  example = "40ceed8a716f08e3f2960a43cb86a9efa9a379ec2f25d673e674bcd212055f30df1b3ff75357154672b62733b7efb275",
     * ),
     *
     * @Assert\NotBlank
     * @Assert\Type("string")
     */
    public string $refresh_token;
}
