<?php

declare(strict_types=1);

namespace App\Auth\Application\DTO;

use App\Common\Lib\Dto\DTO;
use App\Common\Lib\Helpers\DateTimeHelper;
use OpenApi\Annotations as OA;

class AdminSessionForList extends DTO
{
    /**
     * @OA\Property(property = "client_type", type = "string",
     *  example = "android",
     * ),
     */
    public ?string $client_type;

    /**
     * @OA\Property(property = "client_build", type = "string",
     *  example = "55",
     * ),
     */
    public ?string $client_build;

    /**
     * @OA\Property(property = "client_version", type = "string",
     *  example = "1.2.55",
     * ),
     */
    public ?string $client_version;

    /**
     * @OA\Property(property = "created_at", type = "string",
     *  example = "2021-05-19T00:04:03+00:00",
     * )
     */
    public string $created_at;

    /**
     * @OA\Property(property = "updated_at", type = "string",
     *  example = "2021-05-19T00:04:03+00:00",
     * )
     */
    public string $updated_at;

    /**
     * @OA\Property(property = "finished_at", type = "string", nullable = true,
     *  example = "2021-05-19T00:04:03+00:00",
     * )
     */
    public ?string $finished_at;

    /**
     * @return array
     */
    public function jsonSerialize(): array
    {
        $result = parent::jsonSerialize();
        $result['created_at'] = DateTimeHelper::forFrontFromDB($this->created_at);
        $result['updated_at'] = DateTimeHelper::forFrontFromDB($this->updated_at);
        $result['finished_at'] = DateTimeHelper::forFrontFromDB($this->finished_at);

        return $result;
    }
}
