<?php

declare(strict_types=1);

namespace App\Auth\Domain\Service;

use App\Auth\Domain\Entity\User;
use App\Auth\Domain\Exception\UserNotFoundException;
use App\Common\Service\Persistence\DoctrineRepository;
use Doctrine\Persistence\ObjectRepository;
use Ramsey\Uuid\UuidInterface;

class UserRepositoryService
{
    private ObjectRepository $objectRepository;
    private DoctrineRepository $doctrineRepository;

    public function __construct(DoctrineRepository $doctrineRepository)
    {
        $this->objectRepository = $doctrineRepository->getObjectRepository(User::class);
        $this->doctrineRepository = $doctrineRepository;
    }

    public function findOneOrFail(UuidInterface $userUuid): User
    {
        $user = $this->objectRepository->find($userUuid);
        if (!$user instanceof User) {
            throw new UserNotFoundException();
        }
        return $user;
    }

    public function save(User $user): void
    {
        $this->doctrineRepository->save($user);
    }
}
