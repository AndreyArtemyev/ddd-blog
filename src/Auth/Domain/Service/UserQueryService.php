<?php

declare(strict_types=1);

namespace App\Auth\Domain\Service;

use App\Auth\Application\DTO\AdminUserForList;
use App\Common\Lib\Dto\DTOCollection;
use App\Common\Lib\GlobalDto\FilterCommand;
use App\Common\Lib\GlobalDto\FilterCommandCollection;
use App\Common\Lib\GlobalDto\Pagination;
use App\Common\Lib\GlobalDto\Sorting;
use App\Common\Service\Persistence\DoctrineQueryService;
use Doctrine\DBAL\ParameterType;
use function App\Auth\Service\count;

class UserQueryService
{
    private DoctrineQueryService $doctrineQueryService;

    public function __construct(DoctrineQueryService $doctrineQueryService)
    {
        $this->doctrineQueryService = $doctrineQueryService;
    }

    /**
     * @param Pagination $pagination
     * @param FilterCommandCollection $filter
     * @param Sorting $sorting
     * @return DTOCollection<int, AdminUserForList>
     */
    public function adminGetUsers(
        Pagination $pagination,
        FilterCommandCollection $filter,
        Sorting $sorting
    ): DTOCollection {
        ['sql' => $sql, 'parameters' => $parameters] = $this->adminGetUsersQuery($pagination, $filter, $sorting, false);
        return $this->doctrineQueryService->fetchAllDTO($sql, $parameters, AdminUserForList::class);
    }

    /**
     * @param Pagination $pagination
     * @param FilterCommandCollection $filter
     * @return int
     */
    public function adminGetUsersQuantity(
        Pagination $pagination,
        FilterCommandCollection $filter,
        Sorting $sorting
    ): int {
        ['sql' => $sql, 'parameters' => $parameters] = $this->adminGetUsersQuery($pagination, $filter, $sorting, true);
        return $this->doctrineQueryService->fetchInt($sql, $parameters);
    }

    /**
     * @param Pagination $pagination
     * @param FilterCommandCollection $filter
     * @param bool $forQuantity
     * @param Sorting $sorting ,
     * @return array
     */
    private function adminGetUsersQuery(
        Pagination $pagination,
        FilterCommandCollection $filter,
        Sorting $sorting,
        bool $forQuantity
    ): array {
        /*
        select au.uuid     as uuid
             , au.username as username
        from auth_users as au
        where au.username like :searchData
        limit :offset, :row_count
         */

        $parameters = [];

        if ($forQuantity) {
            $select = '1';
        } else {
            $select = '
              au.uuid     as uuid
            , au.username as username
            , au.phone_confirmed_at as phone_confirmed_at
            ';
        }
        $sort = '';

        if ($forQuantity) {
            $limit = '';
        } else {
            $limit = 'limit :offset, :row_count';
            $parameters = array_merge($parameters, $pagination->generateParamForQuery());
        }

        if (!$forQuantity && !empty($sorting->field)) {
            $allowSortColumns = [
                'phone_confirmed_at',
                'username'
            ];

            if (!in_array(strtolower($sorting->field), $allowSortColumns)) {
                throw new \Exception('Order field not allowed');
            }
            $sort = 'order by ' . $sorting->field . ' ' . $sorting->order;
        }

        $whereBuilder = [];
        /** @var FilterCommand $filterCommand */
        foreach ($filter as $filterCommand) {
            switch ($filterCommand->fetchCommand()) {
                case 'search':
                    $whereBuilder[] = 'au.username like :search_data';
                    $parameters['search_data'] = [
                        'value' => "%{$filterCommand->fetchData()}%",
                        'type' => ParameterType::STRING,
                    ];
                    break;
                case 'has_role':
                    $whereBuilder[] = 'exists(select 1 from auth_user_roles where user_uuid = au.uuid)';
                    break;
                case 'phone_is_confirmed':
                    if ((bool)$filterCommand->fetchData() === true) {
                        $whereBuilder[] = 'au.phone_confirmed_at IS NOT NULL';
                    } else {
                        $whereBuilder[] = 'au.phone_confirmed_at IS NULL';
                    }
                    break;
                default:
                    throw new \Exception('Not allowed');
            }
        }

        $where = '';
        if (count($whereBuilder)) {
            $where = 'where (' . implode(') and (', $whereBuilder) . ')';
        }

        $sql = <<<SQL
            select {$select}
            from auth_users as au
            {$where}
            {$sort}
            {$limit}
        SQL;
        if ($forQuantity) {
            $sql = <<<SQL
            select count(*) as quantity from ({$sql}) as tmp
            SQL;
        }


        return [
            'sql' => $sql,
            'parameters' => $parameters,
        ];
    }
}
