<?php

declare(strict_types=1);

namespace App\Auth\Domain\Service;

use Ramsey\Uuid\UuidInterface;
use function App\Auth\Service\count;

class RoleService
{
    public const PERMISSION_READ = 'READ';
    public const PERMISSION_CREATE = 'CREATE';
    public const PERMISSION_UPDATE = 'UPDATE';
    public const PERMISSION_DELETE = 'DELETE';
    public const PERMISSION_ALLOWED = 'ALLOWED';
    public const PERMISSION_ALL = [
        self::PERMISSION_READ,
        self::PERMISSION_CREATE,
        self::PERMISSION_UPDATE,
        self::PERMISSION_DELETE,
        self::PERMISSION_ALLOWED,
    ];

    public const RESOURCE_ADMIN = 'ADMIN';
    public const RESOURCE_ADMIN_ROLE = 'ADMIN_ROLE';
    public const RESOURCE_ADMIN_USER = 'ADMIN_USER';
    public const RESOURCE = [
        self::RESOURCE_ADMIN => [
            'title' => 'Access to admin',
            'allowedPermissions' => [
                self::PERMISSION_ALLOWED,
            ],
        ],
        self::RESOURCE_ADMIN_ROLE => [
            'title' => 'Access to roles',
            'allowedPermissions' => [
                self::PERMISSION_ALLOWED,
            ],
        ],
        self::RESOURCE_ADMIN_USER => [
            'title' => 'Access to users',
            'allowedPermissions' => [
                self::PERMISSION_ALLOWED,
            ],
        ],
    ];


    private RoleQueryService $roleQueryService;
    private bool $isConsole;

    public function __construct(
        RoleQueryService $roleQueryService
    ) {
        $this->roleQueryService = $roleQueryService;
        $this->isConsole = (PHP_SAPI === 'cli');
    }

    public function isGrunted(UuidInterface $userUuid, array $rolePermissions): bool
    {
        $rolePermissionsPlain = [];
        foreach ($rolePermissions as $resource => $rolePermission) {
            foreach ($rolePermission as $permission) {
                $rolePermissionsPlain[$this->normalizeResourceAndPermission($resource, $permission)] = true;
            }
        }
        $userRolePermissions = $this->getUserRolePermissions($userUuid);

        return count(array_intersect_key($rolePermissionsPlain, $userRolePermissions)) === count($rolePermissionsPlain);
    }

    public function getUserRolePermissions(UuidInterface $userUuid): array
    {
        static $cache = [];
        if (!$this->isConsole && isset($cache[$userUuid->toString()])) {
            return $cache[$userUuid->toString()];
        }

        $result = [];
        foreach ($this->roleQueryService->getUserRolePermissions($userUuid) as $row) {
            $result[$this->normalizeResourceAndPermission($row['resource'], $row['permission'])] = true;
        }
        if ($this->isConsole) {
            return $result;
        }

        $cache[$userUuid->toString()] = $result;
        return $cache[$userUuid->toString()];
    }

    public function normalizeResourceAndPermission(string $resource, string $permission): string
    {
        return $resource . '-' . $permission;
    }
}
