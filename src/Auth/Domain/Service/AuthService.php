<?php

declare(strict_types=1);

namespace App\Auth\Domain\Service;

use App\Auth\Domain\Entity\ConfirmationCode;
use App\Auth\Domain\Entity\User;
use App\Auth\Domain\Exception\ConfirmationCodeAttemptsExceededException;
use App\Auth\Domain\Exception\InvalidConfirmationCodeException;
use App\Auth\Domain\Exception\InvalidPhoneNumberException;
use App\Auth\Domain\Exception\TooManySmsException;
use App\Common\Lib\DebugLogger\DebugLogger;
use App\Common\Service\Notification\Sms\SmsNotificationService;
use DateTimeImmutable;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectRepository;
use GuzzleHttp\Client;

class AuthService
{
    public const
        TEST_PIN_CODE = 5555,
        TEST_PHONES =
        [
            // для тестов
            '79789999001',
            '79789999002',
            '79789999003',
            '79789999004',
            '79789999005',
        ],
        EXIST_SECRET = 'gB75%q!p873fV11_',
        COUNTRY_CODE_LIST = ['RU' => '7'];

    protected string $appEnv;
    protected string $smsRequestInterval;
    protected int $attemptLoginCount;
    protected string $codeActiveInterval;
    private Client $httpClient;
    private SmsNotificationService $smsNotificationService;
    private EntityManagerInterface $entityManager;
    private ObjectRepository $userRepository;
    private ConfirmationCodeRepository $confirmationCodeRepository;
    private DebugLogger $debugLogger;

    public function __construct(
        EntityManagerInterface $entityManager,
        SmsNotificationService $smsNotificationService,
        DebugLogger $debugLogger,
        ConfirmationCodeRepository $confirmationCodeRepository,
        string $appEnv,
        string $smsRequestInterval,
        int $attemptLoginCount,
        string $codeActiveInterval
    ) {
        $this->codeActiveInterval = $codeActiveInterval ?: 'PT10M';
        $this->smsRequestInterval = $smsRequestInterval ?: 'PT3M';
        $this->attemptLoginCount = $attemptLoginCount ?: 3;
        $this->appEnv = $appEnv;
        $this->httpClient = new Client();
        $this->entityManager = $entityManager;
        $this->userRepository = $entityManager->getRepository(User::class);
        $this->confirmationCodeRepository = $confirmationCodeRepository;
        $this->smsNotificationService = $smsNotificationService;
        $this->debugLogger = $debugLogger;
    }

    /**
     * @param string $countryCode
     * @param string $phone
     * @return bool
     * @throws InvalidPhoneNumberException
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Exception
     */
    public function sendCode(string $countryCode, string $phone): bool
    {
        $validatedPhone = $this->validatePhone($countryCode, $phone);

        $this->debugLogger->addBreadcrumb(
            self::class . '::validated_phone',
            ['validatedPhone' => $validatedPhone],
            DebugLogger::LEVEL_INFO,
            DebugLogger::CATEGORY_APPLICATION,
        );

        if (!isset($validatedPhone)) {
            throw new InvalidPhoneNumberException();
        }

        $user = $this->userRepository->findOneBy(
            [
                'username' => $validatedPhone
            ]
        );
        if (!$user) {
            $secretPassword = $this->hashPassword(random_bytes(10));
            $user = User::create($validatedPhone, $secretPassword);
            $this->entityManager->persist($user);
        }

        // Удаляем все старые коды подтверждения
        $this->clearConfirmationCodes($validatedPhone);

        $pinCode = $this->isTestPhone($validatedPhone) ? self::TEST_PIN_CODE : random_int(1111, 9999);
        $createdCode = $this->confirmationCodeRepository->findOneActiveByUserName($validatedPhone);

        if (!$createdCode) {
            $hashPassword = $this->hashPassword((string)$pinCode);
            $createdCode = ConfirmationCode::create($validatedPhone, $hashPassword, $this->codeActiveInterval);
        }

        if (!$this->canSend($createdCode)) {
            throw new TooManySmsException();
        }

        $createdCode->setCode($this->hashPassword((string)$pinCode))->update($this->codeActiveInterval);

        if (!$this->isTestPhone($validatedPhone)) {
            $smsResponse = $this->smsNotificationService->send(
                $validatedPhone,
                "Код подтверждения: " . $pinCode,
                $countryCode
            );
        } else {
            $smsResponse = true;
        }

        $this->entityManager->persist($createdCode);
        $this->entityManager->flush();

        return $smsResponse;
    }

    /**
     * @param string $countryCode
     * @param string $phone
     * @param int $code
     * @return User
     * @throws InvalidPhoneNumberException
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \GuzzleHttp\Exception\GuzzleException|InvalidConfirmationCodeException
     * @throws ConfirmationCodeAttemptsExceededException
     */
    public function confirmUser(string $countryCode, string $phone, int $code): User
    {
        $validatedPhone = $this->validatePhone($countryCode, $phone);

        if (!isset($validatedPhone)) {
            throw new InvalidPhoneNumberException();
        }

        /** @var User $user */
        $user = $this->userRepository->findOneBy(
            [
                'username' => $validatedPhone
            ]
        );

        if (!$user) {
            throw new InvalidConfirmationCodeException();
        }

        // Удаляем все старые коды подтверждения
        $this->clearConfirmationCodes($validatedPhone);

        $confirmationCode = $this->confirmationCodeRepository->findOneActiveByUserName($validatedPhone);

        if (!$confirmationCode) {
            throw new InvalidConfirmationCodeException();
        }

        if (!$this->canTry($confirmationCode)) {
            throw new ConfirmationCodeAttemptsExceededException();
        }

        if (password_verify((string)$code, $confirmationCode->getCode())) {
            $isValid = true;
        } else {
            $isValid = false;
            $confirmationCode->addAttempt();
            $this->entityManager->persist($confirmationCode);
            $this->entityManager->flush();
        }

        if (!$isValid) {
            throw new InvalidConfirmationCodeException();
        }

        if (!($user->getPhoneConfirmedAt() instanceof DateTimeImmutable)) {
            $user->setPhoneConfirmedAt();
            $this->entityManager->persist($user);
            $this->entityManager->flush();
        }

        return $user;
    }

    private function clearConfirmationCodes(string $userName): void
    {
        $confirmationCodes = $this->confirmationCodeRepository->findExpiredByUserName($userName);
        foreach ($confirmationCodes as $confirmationCode) {
            $this->entityManager->remove($confirmationCode);
        }
    }

    /**
     * @param $password
     * @return string
     */
    private function hashPassword(string $password): string
    {
        /** @var string|bool|null $hashedPassword */
        $hashedPassword = \password_hash($password, PASSWORD_BCRYPT, ['cost' => 12]);

        if (\is_bool($hashedPassword)) {
            throw new \RuntimeException('Server error hashing password');
        }

        return (string)$hashedPassword;
    }

    private function isTestPhone(string $username): bool
    {
        return $this->appEnv === 'test' || $this->checkTestPhone($username);
    }

    /**
     * @param string $countryCode
     * @param string $phone
     * @return string|null
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    private function validatePhone(string $countryCode, string $phone): ?string
    {
        $countryCodeList = json_decode(json_encode(self::COUNTRY_CODE_LIST), false);
        if (!property_exists($countryCodeList, $countryCode)) {
            return null;
        }
        $countryCodeValue = $countryCodeList->$countryCode;

        //phone number without non-numeric caracters
        $clearPhone = preg_replace("/[^0-9]/", "", $phone);
        $result = null;
        if (strlen($clearPhone) === 10) {
            $result = $countryCodeValue . $clearPhone;
        }
        return $result;
    }

    private function canSend(ConfirmationCode $confirmationCode): bool
    {
        $sentAt = $confirmationCode->getSentAt();
        if (!$sentAt) {
            return true;
        }
        $allowDate = $sentAt->add(new \DateInterval($this->smsRequestInterval));

        $this->debugLogger->addBreadcrumb(
            self::class . '::can_send',
            ['sentAt' => $confirmationCode->getSentAt(), 'allowDate' => $allowDate],
            DebugLogger::LEVEL_INFO,
            DebugLogger::CATEGORY_APPLICATION,
        );

        return $allowDate < new \DateTimeImmutable();
    }

    private function canTry(ConfirmationCode $confirmationCode): bool
    {
        return $confirmationCode->getAttempt() != $this->attemptLoginCount;
    }

    private function checkTestPhone(string $username): bool
    {
        if (in_array($username, self::TEST_PHONES, true)) {
            return true;
        } elseif (preg_match('/^(71789999)[0-9]{3}/', $username)) {
            return true;
        }
        return false;
    }
}
