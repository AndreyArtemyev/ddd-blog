<?php

declare(strict_types=1);

namespace App\Auth\Domain\Service;

use App\Auth\Domain\Entity\Role;
use App\Auth\Domain\Exception\RoleNameNotUniqueException;
use App\Auth\Domain\Exception\RoleNotFoundException;
use App\Auth\Domain\Exception\RoleSlugAndNameNotUniqueException;
use App\Auth\Domain\Exception\RoleSlugNotUniqueException;
use App\Common\Service\Persistence\DoctrineRepository;
use Doctrine\Persistence\ObjectRepository;
use Ramsey\Uuid\UuidInterface;

class RoleRepository
{
    private ObjectRepository $objectRepository;
    private DoctrineRepository $doctrineRepository;

    public function __construct(DoctrineRepository $doctrineRepository)
    {
        $this->objectRepository = $doctrineRepository->getObjectRepository(Role::class);
        $this->doctrineRepository = $doctrineRepository;
    }

    public function findOneOrFail(UuidInterface $roleUuid): Role
    {
        $role = $this->objectRepository->findOneBy(
            [
                'uuid' => $roleUuid,
            ]
        );
        if (!$role instanceof Role) {
            throw new RoleNotFoundException();
        }

        return $role;
    }

    public function findByUuids(array $rolesUuids): array
    {
        return $this->doctrineRepository->getQueryBuilder()
            ->select('r')
            ->from(Role::class, 'r')
            ->andWhere('r.uuid in (:rolesUuids)')
            ->setParameter('rolesUuids', $rolesUuids)
            ->getQuery()
            ->getResult();
    }

    public function delete(Role $role): void
    {
        $this->doctrineRepository->delete($role);
    }

    public function save(Role $role): void
    {
        $this->doctrineRepository->save($role);
    }

    public function isSlugAndNameUnique(string $slug, string $name, ?UuidInterface $roleUuid): void
    {
        $qb = $this->doctrineRepository->getQueryBuilder()
            ->select('r')
            ->from(Role::class, 'r')
            ->andWhere('r.slug LIKE :slug')
            ->andWhere('r.name LIKE :name')
            ->setParameter('slug', $slug)
            ->setParameter('name', $name);

        if ($roleUuid) {
            $qb->andWhere('r.uuid != :roleUuid')
                ->setParameter('roleUuid', $roleUuid);
        }

        /** @var Role|null $role */
        $role = $qb->setMaxResults(1)->getQuery()->getOneOrNullResult();

        if ($role instanceof Role) {
            throw new RoleSlugAndNameNotUniqueException();
        }
    }

    public function isSlugUnique(string $slug, ?UuidInterface $roleUuid): void
    {
        $qb = $this->doctrineRepository->getQueryBuilder()
            ->select('r')
            ->from(Role::class, 'r')
            ->andWhere('r.slug LIKE :slug')
            ->setParameter('slug', $slug);

        if ($roleUuid) {
            $qb->andWhere('r.uuid != :roleUuid')
                ->setParameter('roleUuid', $roleUuid);
        }

        /** @var Role|null $role */
        $role = $qb->setMaxResults(1)->getQuery()->getOneOrNullResult();
        if ($role instanceof Role) {
            throw new RoleSlugNotUniqueException();
        }
    }

    public function isNameUnique(string $name, ?UuidInterface $roleUuid): void
    {
        $qb = $this->doctrineRepository->getQueryBuilder()
            ->select('r')
            ->from(Role::class, 'r')
            ->andWhere('r.name LIKE :name')
            ->setParameter('name', $name);

        if ($roleUuid) {
            $qb->andWhere('r.uuid != :roleUuid')
                ->setParameter('roleUuid', $roleUuid);
        }

        /** @var Role|null $role */
        $role = $qb->setMaxResults(1)->getQuery()->getOneOrNullResult();
        if ($role instanceof Role) {
            throw new RoleNameNotUniqueException();
        }
    }

    public function checkNameAndSlug(string $slug, string $name, ?UuidInterface $roleUuid = null): void
    {
        $this->isSlugAndNameUnique($slug, $name, $roleUuid);
        $this->isNameUnique($name, $roleUuid);
        $this->isSlugUnique($slug, $roleUuid);
    }
}
