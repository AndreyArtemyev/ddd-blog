<?php

declare(strict_types=1);

namespace App\Auth\Domain\Service;

use App\Auth\Domain\Entity\RolePermission;
use App\Common\Service\Persistence\DoctrineRepository;
use Doctrine\Persistence\ObjectRepository;

class RolePermissionRepository
{
    private ObjectRepository $objectRepository;
    private DoctrineRepository $doctrineRepository;

    public function __construct(DoctrineRepository $doctrineRepository)
    {
        $this->objectRepository = $doctrineRepository->getObjectRepository(RolePermission::class);
        $this->doctrineRepository = $doctrineRepository;
    }

    /**
     * @return array<int, RolePermission>
     */
    public function findAll(): array
    {
        return $this->doctrineRepository->getQueryBuilder()
            ->select('rp')
            ->from(RolePermission::class, 'rp')
            ->addOrderBy('rp.resource', 'asc')
            ->addOrderBy('rp.permission', 'asc')
            ->getQuery()
            ->getResult();
    }

    public function findByUuids(array $rolePermissionUuids): array
    {
        return $this->doctrineRepository->getQueryBuilder()
            ->select('rp')
            ->from(RolePermission::class, 'rp')
            ->andWhere('rp.uuid in (:rolePermissionUuids)')
            ->setParameter('rolePermissionUuids', $rolePermissionUuids)
            ->getQuery()
            ->getResult();
    }
}
