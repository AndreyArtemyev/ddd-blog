<?php

declare(strict_types=1);

namespace App\Auth\Domain\Service;

use App\Auth\Application\DTO\AdminSessionForList;
use App\Common\Lib\Dto\DTOCollection;
use App\Common\Lib\GlobalDto\Pagination;
use App\Common\Lib\GlobalDto\Sorting;
use App\Common\Service\Persistence\DoctrineQueryService;
use Ramsey\Uuid\UuidInterface;

class SessionQueryService
{
    private DoctrineQueryService $doctrineQueryService;

    public function __construct(DoctrineQueryService $doctrineQueryService)
    {
        $this->doctrineQueryService = $doctrineQueryService;
    }

    public function adminGetSessionsByUserUuidForList(
        UuidInterface $userUuid,
        Pagination $pagination,
        Sorting $sorting
    ): DTOCollection {
        ['sql' => $sql, 'parameters' => $parameters] = $this->adminGetSessionsByUserUuidQuery(
            $userUuid,
            $pagination,
            $sorting,
            false
        );
        return $this->doctrineQueryService->fetchAllDTO($sql, $parameters, AdminSessionForList::class);
    }

    public function adminGetSessionsByUserUuidQuantity(
        UuidInterface $userUuid,
        Pagination $pagination,
        Sorting $sorting
    ): int {
        ['sql' => $sql, 'parameters' => $parameters] = $this->adminGetSessionsByUserUuidQuery(
            $userUuid,
            $pagination,
            $sorting,
            true
        );
        return $this->doctrineQueryService->fetchInt($sql, $parameters);
    }

    private function adminGetSessionsByUserUuidQuery(
        UuidInterface $userUuid,
        Pagination $pagination,
        Sorting $sorting,
        bool $forQuantity
    ): array {
        $parameters = [];
        $parameters['user_uuid'] = $userUuid;

        $select = 'count(ss.uuid) as quantity';
        $sort = '';
        $limit = '';

        if (!$forQuantity) {
            $select = '
               ss.client_type as client_type
             , ss.client_build as client_build
             , ss.client_version as client_version
             , ss.created_at as created_at
             , ss.updated_at as updated_at
             , ss.finished_at as finished_at
            ';

            $limit = 'limit :offset, :row_count';
            $parameters = array_merge($parameters, $pagination->generateParamForQuery());

            if (!empty($sorting->field)) {
                $allowSortColumns = [
                    'client_type',
                    'client_build',
                    'client_version',
                    'created_at',
                    'updated_at',
                    'finished_at',
                ];

                if (!in_array(strtolower($sorting->field), $allowSortColumns)) {
                    throw new \Exception('Order field not allowed');
                }

                $sort = 'order by ' . $sorting->field . ' ' . $sorting->order;
            } else {
                $sort = 'order by created_at desc';
            }
        }

        $where = 'where ss.user_uuid = :user_uuid';

        $sql = <<<SQL
            select {$select}
            from auth_sessions as ss
            {$where}
            {$sort}
            {$limit}
        SQL;

        return [
            'sql' => $sql,
            'parameters' => $parameters,
        ];
    }
}
