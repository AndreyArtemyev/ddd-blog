<?php

declare(strict_types=1);

namespace App\Auth\Domain\Service;

use App\Auth\Domain\Entity\Session;
use App\Auth\Domain\Entity\User;
use App\Common\Service\Persistence\DoctrineRepository;
use Doctrine\Persistence\ObjectRepository;
use Ramsey\Uuid\UuidInterface;

class SessionRepository
{
    private DoctrineRepository $doctrineRepository;
    private ObjectRepository $objectRepository;
    private bool $sessionSingle;

    public function __construct(DoctrineRepository $doctrineRepository, string $sessionSingle)
    {
        $this->doctrineRepository = $doctrineRepository;
        $this->objectRepository = $doctrineRepository->getObjectRepository(Session::class);
        $this->sessionSingle = ($sessionSingle === '1');
    }

    public function findOneActiveSession(UuidInterface $uuid): ?Session
    {
        /** @var Session $session */
        $session = $this->objectRepository->findOneBy(
            [
                'uuid'       => $uuid,
                'finishedAt' => null
            ]
        );

        return $session;
    }

    public function findActiveSessionsByUserUuid(UuidInterface $userUuid): array
    {
        /** @var Session[] $session */
        $sessions = $this->objectRepository->findBy(
            [
                'userUuid'   => $userUuid,
                'finishedAt' => null
            ]
        );

        return $sessions;
    }

    public function findOneActiveSessionByRefreshToken(string $refreshToken): ?Session
    {
        /** @var Session $session */
        $session = $this->objectRepository->findOneBy(
            [
                'refreshToken' => $refreshToken,
                'finishedAt'   => null
            ]
        );

        return $session;
    }

    public function createSession(
        User $user,
        ?string $clientType,
        ?string $clientVersion,
        ?string $clientBuild
    ): Session {
        if ($this->sessionSingle) {
            $this->finishSessionsByUserUuid($user->getUuid());
        }

        $session = new Session($user, $clientType, $clientVersion, $clientBuild);
        $this->save($session);

        return $session;
    }

    public function save(Session ...$sessions): void
    {
        $this->doctrineRepository->save(...$sessions);
    }

    private function finishSessionsByUserUuid(UuidInterface $userUuid): void
    {
        $sessions = $this->findActiveSessionsByUserUuid($userUuid);

        if (!empty($sessions)) {
            /** @var Session $session */
            foreach ($sessions as $session) {
                $session->finish();
            }

            $this->save(...$sessions);
        }
    }
}
