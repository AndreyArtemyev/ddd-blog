<?php

declare(strict_types=1);

namespace App\Auth\Domain\Service;

use App\Auth\Domain\Entity\ConfirmationCode;
use App\Common\Service\Persistence\DoctrineRepository;
use Doctrine\Persistence\ObjectRepository;

class ConfirmationCodeRepository
{
    private ObjectRepository $objectRepository;
    private DoctrineRepository $doctrineRepository;

    public function __construct(DoctrineRepository $doctrineRepository)
    {
        $this->objectRepository = $doctrineRepository->getObjectRepository(ConfirmationCode::class);
        $this->doctrineRepository = $doctrineRepository;
    }

    public function findExpiredByUserName(string $userName): array
    {
        return $this->doctrineRepository->getQueryBuilder()
            ->select("cc")
            ->from(ConfirmationCode::class, "cc")
            ->where("cc.valid < :valid")
            ->andWhere("cc.username = :username")->setParameter("username", $userName)
            ->setParameter("valid", new \DateTimeImmutable())
            ->getQuery()->getResult();
    }

    public function findOneActiveByUserName(string $userName): ?ConfirmationCode
    {
        return $this->doctrineRepository->getQueryBuilder()
            ->select("cc")
            ->from(ConfirmationCode::class, "cc")
            ->where("cc.valid > :valid")
            ->andWhere("cc.username = :username")->setParameter("username", $userName)
            ->setParameter("valid", new \DateTimeImmutable())
            ->setMaxResults(1)
            ->getQuery()->getOneOrNullResult();
    }
}
