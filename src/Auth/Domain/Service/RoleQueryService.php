<?php

declare(strict_types=1);

namespace App\Auth\Domain\Service;

use App\Common\Service\Persistence\DoctrineQueryService;
use Ramsey\Uuid\UuidInterface;

class RoleQueryService
{
    private DoctrineQueryService $doctrineQueryService;

    public function __construct(DoctrineQueryService $doctrineQueryService)
    {
        $this->doctrineQueryService = $doctrineQueryService;
    }

    public function getUserRolePermissions(UuidInterface $userUuid): array
    {
        $sql =
            <<<SQL
            select distinct arp.resource as resource, arp.permission as permission
            from auth_role_permissions as arp
                     inner join auth_role_role_permissions arrp on arp.uuid = arrp.role_permission_uuid
                     inner join auth_roles ar on arrp.role_uuid = ar.uuid
                     inner join auth_user_roles aur on ar.uuid = aur.role_uuid
            where aur.user_uuid = :userUuid
            order by arp.resource, arp.permission
            SQL;

        $parameters = [
            'userUuid' => $userUuid,
        ];

        return $this->doctrineQueryService->fetchAllAssociative($sql, $parameters);
    }

    public function getRoles(): array
    {
        $result = [];

        $roleSql =
            <<<SQL
            select uuid
                 , name
                 , slug
                 , description
            from auth_roles
            order by name
            SQL;
        foreach ($this->doctrineQueryService->fetchAllAssociative($roleSql) as $role) {
            $result[$role['uuid']] = [
                'uuid' => $role['uuid'],
                'name' => $role['name'],
                'slug' => $role['slug'],
                'description' => $role['description'],
                'permissions' => [],
            ];
        }
        if (empty($result)) {
            return $result;
        }

        $roleRolePermissionSql =
            <<<SQL
            select role_uuid
                 , role_permission_uuid
            from auth_role_role_permissions;
            SQL;
        $rolesByPermissions = [];
        foreach ($this->doctrineQueryService->fetchAllAssociative($roleRolePermissionSql) as $roleRolePermission) {
            $rolesByPermissions[$roleRolePermission['role_permission_uuid']][$roleRolePermission['role_uuid']] = true;
        }

        $rolePermissionSql =
            <<<SQL
            select uuid
                 , resource
                 , permission
            from auth_role_permissions
            order by resource, permission
            SQL;
        foreach ($this->doctrineQueryService->fetchAllAssociative($rolePermissionSql) as $rolePermission) {
            if (empty($rolesByPermissions[$rolePermission['uuid']])) {
                continue;
            }

            foreach ($rolesByPermissions[$rolePermission['uuid']] as $roleUuid => $bool) {
                $result[$roleUuid]['permissions'][] = $rolePermission;
            }
        }

        return array_values($result);
    }
}
