<?php

declare(strict_types=1);

namespace App\Auth\Domain\Entity;

use App\Common\Lib\Doctrine\TimestampableImmutableTrait;
use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

/**
 * @ORM\Entity()
 * @ORM\Table(name="auth_sessions")
 */
class Session
{
    use TimestampableImmutableTrait;

    public const REFRESH_TOKEN_NAME = 'refresh_token';

    /**
     * @var UuidInterface
     * @ORM\Id
     * @ORM\Column(name="uuid", type="uuid", unique=true)
     */
    private UuidInterface $uuid;

    /**
     * @var UuidInterface
     * @ORM\Column(name="user_uuid", type="uuid")
     */
    private UuidInterface $userUuid;

    /**
     * @var string
     * @ORM\Column(name="refresh_token", type="string", nullable=true)
     */
    private ?string $refreshToken;

    /**
     * @var string|null
     * @ORM\Column(name="client_type", type="string", nullable=true)
     */
    private ?string $clientType;

    /**
     * @var string|null
     * @ORM\Column(name="client_build", type="string", nullable=true)
     */
    private ?string $clientBuild;

    /**
     * @var string|null
     * @ORM\Column(name="client_version", type="string", nullable=true)
     */
    private ?string $clientVersion;

    /**
     * @var DateTimeImmutable|null
     * @ORM\Column(name="finished_at", type="datetime_immutable", nullable=true)
     */
    private ?DateTimeImmutable $finishedAt;

    public function __construct(User $user, ?string $clientType, ?string $clientVersion, ?string $clientBuild)
    {
        $this->uuid = Uuid::uuid4();
        $this->userUuid = $user->getUuid();
        $this->clientType = $clientType;
        $this->clientVersion = $clientVersion;
        $this->clientBuild = $clientBuild;
    }

    /**
     * @return UuidInterface
     */
    public function getUuid(): UuidInterface
    {
        return $this->uuid;
    }

    /**
     * @return UuidInterface
     */
    public function getUserUuid(): UuidInterface
    {
        return $this->userUuid;
    }

    /**
     * @param string|null $refreshToken
     */
    public function setRefreshToken(?string $refreshToken): void
    {
        $this->refreshToken = $refreshToken;
    }

    public function finish(): void
    {
        $this->finishedAt = new DateTimeImmutable();
    }
}
