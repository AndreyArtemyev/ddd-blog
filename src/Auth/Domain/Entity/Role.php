<?php

declare(strict_types=1);

namespace App\Auth\Domain\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

/**
 * @ORM\Entity()
 * @ORM\Table(name="auth_roles")
 */
class Role
{
    public const TABLE_NAME = 'auth_roles';
    public const TABLE_NAME_ROLE_PERMISSION = 'auth_role_role_permissions';

    /**
     * @var UuidInterface
     * @ORM\Id
     * @ORM\Column(name="uuid", type="uuid", unique=true)
     */
    private UuidInterface $uuid;

    /**
     * @var string
     * @ORM\Column(name="name", type="string", length=255, unique=true, nullable=false)
     */
    private string $name;

    /**
     * @var string
     * @ORM\Column(name="slug", type="string", length=255, unique=true, nullable=false)
     */
    private string $slug;

    /**
     * @var string
     * @ORM\Column(name="description", type="text", nullable=false)
     */
    private string $description;

    /**
     * @var Collection|RolePermission[]
     * @psalm-var Collection<int, RolePermission>
     * @ORM\ManyToMany(targetEntity="RolePermission")
     * @ORM\JoinTable(name="auth_role_role_permissions",
     *  joinColumns={@ORM\JoinColumn(name="role_uuid", referencedColumnName="uuid", onDelete="cascade")},
     *  inverseJoinColumns={
     *     @ORM\JoinColumn(name="role_permission_uuid", referencedColumnName="uuid", onDelete="cascade")
     *  }
     * )
     * @ORM\OrderBy({"resource" = "ASC", "permission" = "ASC"})
     */
    private $permissions;

    private function __construct()
    {
        $this->uuid = Uuid::uuid4();
    }

    public static function create(
        string $name,
        string $slug,
        string $description,
        array $permissions
    ): self {
        $role = new self();
        $role->name = $name;
        $role->slug = $slug;
        $role->description = $description;
        $role->setPermission($permissions);

        return $role;
    }

    public function update(
        string $name,
        string $slug,
        string $description,
        array $permissions
    ): void {
        $this->name = $name;
        $this->slug = $slug;
        $this->description = $description;
        $this->setPermission($permissions);
    }

    private function setPermission(array $newPermissions): void
    {
        if (!$this->permissions instanceof Collection) {
            $this->permissions = new ArrayCollection();
        }

        $newPermissionsByUuids = [];
        /** @var RolePermission $newPermission */
        foreach ($newPermissions as $newPermission) {
            $newPermissionsByUuids[$newPermission->getUuid()->toString()] = $newPermission;
        }

        $oldPermissionsByUuid = [];
        foreach ($this->permissions as $key => $permission) {
            if (!isset($newPermissionsByUuids[$permission->getUuid()->toString()])) {
                $this->permissions->remove($key);
                continue;
            }
            $oldPermissionsByUuid[$permission->getUuid()->toString()] = $key;
        }

        /** @var RolePermission $newPermission */
        foreach ($newPermissions as $newPermission) {
            if (isset($oldPermissionsByUuid[$newPermission->getUuid()->toString()])) {
                continue;
            }

            $this->permissions->add($newPermission);
        }
    }

    // getters
    public function getUuid(): UuidInterface
    {
        return $this->uuid;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getSlug(): string
    {
        return $this->slug;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @return RolePermission[]|Collection
     */
    public function getPermissions()
    {
        return $this->permissions;
    }
}
