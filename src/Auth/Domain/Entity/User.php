<?php

namespace App\Auth\Domain\Entity;

use App\Common\Lib\User\CommonUserInterface;
use DateTimeImmutable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Table(name="auth_users")
 * @ORM\Entity()
 */
class User implements CommonUserInterface
{
    public const TABLE_NAME = 'auth_users';
    public const TABLE_NAME_ROLE = 'auth_user_roles';

    /**
     * @var UuidInterface
     * @ORM\Id
     * @ORM\Column(name="uuid", type="uuid", unique=true)
     */
    private UuidInterface $uuid;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     */
    private string $username;

    /**
     * @var Collection|Role[]
     * @psalm-var Collection<int, Role>
     * @ORM\ManyToMany(targetEntity="Role")
     * @ORM\JoinTable(name="auth_user_roles",
     *  joinColumns={@ORM\JoinColumn(name="user_uuid", referencedColumnName="uuid", onDelete="cascade")},
     *  inverseJoinColumns={@ORM\JoinColumn(name="role_uuid", referencedColumnName="uuid", onDelete="cascade")}
     * )
     * @ORM\OrderBy({"name" = "ASC"})
     */
    private $roles;

    /**
     * @var DateTimeImmutable|null
     * @ORM\Column(name="phone_confirmed_at", type="datetime_immutable", nullable=true)
     */
    private ?DateTimeImmutable $phoneConfirmedAt = null;

    /**
     * @ORM\Column(type="string")
     */
    private string $password;

    /**
     * @var DateTimeImmutable
     * @Gedmo\Mapping\Annotation\Timestampable(on="create")
     * @Doctrine\ORM\Mapping\Column(type="datetime_immutable", options={"default": "CURRENT_TIMESTAMP"})
     */
    private DateTimeImmutable $createdAt;

    /**
     * @var DateTimeImmutable
     * @Gedmo\Mapping\Annotation\Timestampable(on="update")
     * @Doctrine\ORM\Mapping\Column(type="datetime_immutable", options={"default": "CURRENT_TIMESTAMP"})
     */
    private DateTimeImmutable $updatedAt;

    private function __construct()
    {
        $this->uuid = Uuid::uuid4();
    }

    public static function create(string $username, string $password): self
    {
        $user = new self();
        $user->username = $username;
        $user->password = $password;
        return $user;
    }

    public function update(array $roles): void
    {
        $this->setRoles($roles);
    }

    public function getUuid(): UuidInterface
    {
        return $this->uuid;
    }

    /**
     * A visual identifier that represents this user.
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return $this->username;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Используется symfony, в нашем коде надо использовать: getFullRoles
     * @see UserInterface
     */
    public function getRoles(): array
    {
        // guarantee every user at least has ROLE_USER
        return ['ROLE_USER'];
    }

    /**
     * @return Role[]|Collection
     */
    public function getFullRoles()
    {
        return $this->roles;
    }

    private function setRoles(array $newRoles): void
    {
        if (!$this->roles instanceof Collection) {
            $this->roles = new ArrayCollection();
        }

        $newRolesByUuids = [];
        /** @var Role $newRole */
        foreach ($newRoles as $newRole) {
            $newRolesByUuids[$newRole->getUuid()->toString()] = $newRole;
        }

        $oldRolesByUuid = [];
        foreach ($this->roles as $key => $role) {
            if (!isset($newRolesByUuids[$role->getUuid()->toString()])) {
                $this->roles->remove($key);
                continue;
            }
            $oldRolesByUuid[$role->getUuid()->toString()] = $key;
        }

        /** @var Role $newRole */
        foreach ($newRoles as $newRole) {
            if (isset($oldRolesByUuid[$newRole->getUuid()->toString()])) {
                continue;
            }

            $this->roles->add($newRole);
        }
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Returning a salt is only needed, if you are not using a modern
     * hashing algorithm (e.g. bcrypt or sodium) in your security.yaml.
     * @see UserInterface
     */
    public function getSalt(): ?string
    {
        return null;
    }

    /**
     * @return void
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getPhoneConfirmedAt(): ?DateTimeImmutable
    {
        return $this->phoneConfirmedAt;
    }

    public function setPhoneConfirmedAt(): void
    {
        $this->phoneConfirmedAt = new DateTimeImmutable();
    }
}
