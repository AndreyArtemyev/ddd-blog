<?php

declare(strict_types=1);

namespace App\Auth\Domain\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="auth_confirmation_codes")
 * @ORM\Entity()
 */
class ConfirmationCode
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", name="username", length=180)
     */
    private $username;

    /**
     * @ORM\Column(type="string", name="code", length=255, unique=true)
     */
    private $code;

    /**
     * @ORM\Column(type="integer", name="attempt", nullable=false)
     */
    private $attempt;

    /**
     * @ORM\Column(type="datetime_immutable", name="valid")
     */
    private $valid;

    /**
     * @ORM\Column(type="datetime_immutable", name="sent_at", nullable=false, options={"default": "CURRENT_TIMESTAMP"})
     */
    private $sentAt;

    public static function create(string $username, string $hashedCode, string $codeActiveInterval): self
    {
        $confirmationCode = new self();

        $confirmationCode->username = $username;
        $confirmationCode->code = $hashedCode;
        $confirmationCode->valid = (new \DateTimeImmutable())->add(new \DateInterval($codeActiveInterval));
        $confirmationCode->attempt = 0;

        return $confirmationCode;
    }

    public function getCode(): string
    {
        return $this->code;
    }

    public function getSentAt(): ?\DateTimeImmutable
    {
        return $this->sentAt;
    }

    public function getAttempt(): int
    {
        return $this->attempt;
    }

    public function update(string $codeActiveInterval): void
    {
        $this->valid = (new \DateTimeImmutable())->add(new \DateInterval($codeActiveInterval));
        $this->sentAt = new \DateTimeImmutable();
        $this->attempt = 0;
    }

    public function setCode(string $code): self
    {
        $this->code = $code;

        return $this;
    }

    public function addAttempt(): void
    {
        $this->attempt++;
    }
}
