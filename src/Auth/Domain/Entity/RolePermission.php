<?php

declare(strict_types=1);

namespace App\Auth\Domain\Entity;

use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\UuidInterface;

/**
 * @ORM\Entity()
 * @ORM\Table(name="auth_role_permissions")
 */
class RolePermission
{
    /**
     * @var UuidInterface
     * @ORM\Id
     * @ORM\Column(name="uuid", type="uuid", unique=true)
     */
    private UuidInterface $uuid;

    /**
     * @var string
     * @ORM\Column(name="resource", type="string", length=255, nullable=false)
     */
    private string $resource;

    /**
     * @var string
     * @ORM\Column(name="permission", type="string", length=255, nullable=false)
     */
    private string $permission;

    private function __construct()
    {
        throw new \Exception('Only by migration');
    }

    public function getUuid(): UuidInterface
    {
        return $this->uuid;
    }

    public function getResource(): string
    {
        return $this->resource;
    }

    public function getPermission(): string
    {
        return $this->permission;
    }
}
