<?php

declare(strict_types=1);

namespace App\Auth\Domain\Exception;

class RoleSlugNotUniqueException extends \DomainException
{
    protected $code = DomainExceptionCode::ROLE_SLUG_NOT_UNIQUE;
    protected $message = 'Роль с таким слагом существует';
}
