<?php

declare(strict_types=1);

namespace App\Auth\Domain\Exception;

use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;

class ActiveSessionNotFoundException extends UnauthorizedHttpException
{
}
