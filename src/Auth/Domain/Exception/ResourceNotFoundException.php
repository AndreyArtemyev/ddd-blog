<?php

declare(strict_types=1);

namespace App\Auth\Domain\Exception;

class ResourceNotFoundException extends \DomainException
{
    protected $code = DomainExceptionCode::RESOURCE_NOT_FOUND;
    protected $message = 'Ресурс не найден';
}
