<?php

declare(strict_types=1);

namespace App\Auth\Domain\Exception;

class ConfirmationCodeAttemptsExceededException extends \DomainException
{
    protected $code = DomainExceptionCode::CONFIRMATION_CODE_ATTEMPTS_EXCEEDED;
    protected $message = 'Превышено количество попыток проверки кода';
}
