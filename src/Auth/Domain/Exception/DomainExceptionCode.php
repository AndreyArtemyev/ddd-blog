<?php

declare(strict_types=1);

namespace App\Auth\Domain\Exception;

class DomainExceptionCode
{
    public const INVALID_CONFIRMATION_CODE = 1001;
    public const CONFIRMATION_CODE_ATTEMPTS_EXCEEDED = 1002;
    public const TOO_MANY_SMS = 1003;
    public const INVALID_PHONE_NUMBER = 1004;
    public const PERMISSION_NOT_ALLOWED = 1005;
    public const RESOURCE_NOT_FOUND = 1006;
    public const ROLE_NOT_FOUND = 1007;
    public const USER_NOT_FOUND = 1008;

    public const ROLE_SLUG_AND_NAME_NOT_UNIQUE = 2060;
    public const ROLE_SLUG_NOT_UNIQUE = 2061;
    public const ROLE_NAME_NOT_UNIQUE = 2062;
}
