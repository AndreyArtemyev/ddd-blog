<?php

declare(strict_types=1);

namespace App\Auth\Domain\Exception;

class TooManySmsException extends \DomainException
{
    protected $code = DomainExceptionCode::TOO_MANY_SMS;
    protected $message = 'Превышена частота отправки смс';
}
