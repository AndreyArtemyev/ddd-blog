<?php

declare(strict_types=1);

namespace App\Auth\Domain\Exception;

class RoleNotFoundException extends \DomainException
{
    protected $code = DomainExceptionCode::ROLE_NOT_FOUND;
    protected $message = 'Роль не найдена';
}
