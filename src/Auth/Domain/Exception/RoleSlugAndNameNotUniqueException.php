<?php

declare(strict_types=1);

namespace App\Auth\Domain\Exception;

class RoleSlugAndNameNotUniqueException extends \DomainException
{
    protected $code = DomainExceptionCode::ROLE_SLUG_AND_NAME_NOT_UNIQUE;
    protected $message = 'Роль с таким наименованием и слагом существует';
}
