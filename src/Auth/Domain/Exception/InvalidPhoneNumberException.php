<?php

declare(strict_types=1);

namespace App\Auth\Domain\Exception;

class InvalidPhoneNumberException extends \DomainException
{
    protected $code = DomainExceptionCode::INVALID_PHONE_NUMBER;
    protected $message = 'Недопустимый телефонный номер';
}
