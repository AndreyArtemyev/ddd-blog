<?php

declare(strict_types=1);

namespace App\Auth\Domain\Exception;

class InvalidConfirmationCodeException extends \DomainException
{
    protected $code = DomainExceptionCode::INVALID_CONFIRMATION_CODE;
    protected $message = 'Пожалуйста, введите корректный код, отправленный вам по SMS';
}
