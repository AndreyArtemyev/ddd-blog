<?php

declare(strict_types=1);

namespace App\Auth\Domain\Exception;

class UserNotFoundException extends \DomainException
{
    protected $code = DomainExceptionCode::USER_NOT_FOUND;
    protected $message = 'Пользователь не найден';
}
