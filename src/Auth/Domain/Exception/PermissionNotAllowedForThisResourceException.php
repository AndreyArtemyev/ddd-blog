<?php

declare(strict_types=1);

namespace App\Auth\Domain\Exception;

class PermissionNotAllowedForThisResourceException extends \DomainException
{
    protected $code = DomainExceptionCode::PERMISSION_NOT_ALLOWED;
    protected $message = 'Отсутствует разрешение для этого ресурса';
}
