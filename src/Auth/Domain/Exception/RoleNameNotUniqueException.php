<?php

declare(strict_types=1);

namespace App\Auth\Domain\Exception;

class RoleNameNotUniqueException extends \DomainException
{
    protected $code = DomainExceptionCode::ROLE_NAME_NOT_UNIQUE;
    protected $message = 'Роль с таким наименованием существует';
}
