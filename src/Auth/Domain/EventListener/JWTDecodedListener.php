<?php

declare(strict_types=1);

namespace App\Auth\Domain\EventListener;

use App\Auth\Domain\Entity\Session;
use App\Auth\Domain\Service\SessionRepository;
use Lexik\Bundle\JWTAuthenticationBundle\Event\JWTDecodedEvent;
use Ramsey\Uuid\Exception\InvalidUuidStringException;
use Ramsey\Uuid\Uuid;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

class JWTDecodedListener
{
    private RequestStack $requestStack;
    private SessionRepository $sessionRepository;

    public function __construct(RequestStack $requestStack, SessionRepository $sessionRepository)
    {
        $this->requestStack = $requestStack;
        $this->sessionRepository = $sessionRepository;
    }

    public function onJWTDecoded(JWTDecodedEvent $event): void
    {
        $payload = $event->getPayload();

        if (empty($payload['sessionUuid']) || !is_string($payload['sessionUuid'])) {
            $event->markAsInvalid();
            return;
        }

        try {
            $sessionUuid = Uuid::fromString($payload['sessionUuid']);
        } catch (InvalidUuidStringException $exception) {
            $event->markAsInvalid();
            return;
        }

        $activeSession = $this->sessionRepository->findOneActiveSession($sessionUuid);

        if (!($activeSession instanceof Session)) {
            $event->markAsInvalid();
            return;
        }

        $request = $this->requestStack->getCurrentRequest();

        if ($request instanceof Request) {
            $request->request->set('sessionUuid', $sessionUuid->toString());
        }
    }
}
