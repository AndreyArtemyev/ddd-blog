<?php

declare(strict_types=1);

namespace App\Auth\Domain\EventListener;

use App\Auth\Domain\Service\RoleService;
use App\Common\Lib\Controller\AdminApiController;
use Symfony\Component\HttpKernel\Event\ControllerEvent;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

//TODO to auth bundle (after refactoring to role bundle)
class ControllerCheckPermission
{
    protected RoleService $roleService;

    public function __construct(RoleService $roleService)
    {
        $this->roleService = $roleService;
    }

    public function onKernelController(ControllerEvent $event): void
    {
        $controller = $event->getController();
        if (is_array($controller)) {
            $controller = $controller[0];
        }

        if ($controller instanceof AdminApiController) {
            $rolesPermissions = array_merge(
                [
                    RoleService::RESOURCE_ADMIN => [
                        RoleService::PERMISSION_ALLOWED
                    ]
                ],
                $controller::rolePermissions()
            );

            if (!$this->roleService->isGrunted($controller->getUserUuid(), $rolesPermissions)) {
                throw new AccessDeniedException();
            }
        }
    }
}
