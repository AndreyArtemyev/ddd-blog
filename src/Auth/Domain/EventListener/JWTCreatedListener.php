<?php

declare(strict_types=1);

namespace App\Auth\Domain\EventListener;

use App\Common\Lib\User\CommonUserInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Event\JWTCreatedEvent;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

class JWTCreatedListener
{
    private RequestStack $requestStack;

    public function __construct(RequestStack $requestStack)
    {
        $this->requestStack = $requestStack;
    }

    public function onJWTCreated(JWTCreatedEvent $event): void
    {
        $user = $event->getUser();
        if (!$user instanceof CommonUserInterface) {
            return;
        }

        $payload = $event->getData();
        $payload['sub'] = $user->getUuid();

        $request = $this->requestStack->getCurrentRequest();

        if ($request instanceof Request) {
            $sessionUuid = $request->request->get('sessionUuid');

            if ($sessionUuid) {
                $payload['sessionUuid'] = $sessionUuid;
            }
        }

        $event->setData($payload);
    }
}
