<?php

declare(strict_types=1);

namespace App\Auth\Domain\EventListener;

use App\Auth\Domain\Entity\Session;
use App\Auth\Domain\Service\SessionRepository;
use Lexik\Bundle\JWTAuthenticationBundle\Event\AuthenticationSuccessEvent;
use Lexik\Bundle\JWTAuthenticationBundle\Exception\InvalidTokenException;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWSProvider\JWSProviderInterface;
use Ramsey\Uuid\Uuid;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Core\User\UserInterface;

class AuthenticationSuccessEventListener
{
    private JWSProviderInterface $jwsProvider;
    private RequestStack $requestStack;
    private SessionRepository $sessionRepository;

    public function __construct(
        JWSProviderInterface $jwsProvider,
        RequestStack $requestStack,
        SessionRepository $sessionRepository
    ) {
        $this->jwsProvider = $jwsProvider;
        $this->requestStack = $requestStack;
        $this->sessionRepository = $sessionRepository;
    }

    public function onAuthenticationSuccess(AuthenticationSuccessEvent $event): void
    {
        $data = $event->getData();
        $user = $event->getUser();
        $request = $this->requestStack->getCurrentRequest();
        $token = $data['token'] ?? null;

        if (!($user instanceof UserInterface)  || !($request instanceof Request) || empty($token)) {
            return;
        }

        $jwt = $this->jwsProvider->load($token);
        $payload = $jwt->getPayload();
        $sessionUuid = Uuid::fromString((string)$payload['sessionUuid']);
        $activeSession = $this->sessionRepository->findOneActiveSession($sessionUuid);

        if (!($activeSession instanceof Session)) {
            throw new InvalidTokenException();
        }

        $refreshTokenString = bin2hex(openssl_random_pseudo_bytes(64));
        $activeSession->setRefreshToken($refreshTokenString);
        $this->sessionRepository->save($activeSession);
        $data[Session::REFRESH_TOKEN_NAME] = $refreshTokenString;

        $event->setData($data);
    }
}
