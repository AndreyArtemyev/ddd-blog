<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211222040721 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE blog_posts (
        uuid CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', 
        user_uuid CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', 
        alias LONGTEXT NOT NULL, 
        body LONGTEXT NOT NULL, 
        alias_picture_uuid CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:uuid)\', 
        date_posted DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', 
        created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', 
        updated_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', 
        UNIQUE INDEX UNIQ_2BC3B20TD17F50A7 (uuid), 
        INDEX IDX_2BC3B20TD17F50A7 (user_uuid), 
        PRIMARY KEY(uuid)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql(
            'ALTER TABLE
          blog_posts
        ADD
          CONSTRAINT FK_20211221124703 FOREIGN KEY (user_uuid) REFERENCES auth_users (uuid)'
        );
        $this->addSql('CREATE TABLE blog_comments (
        uuid CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', 
        post_uuid CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', 
        user_uuid CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', 
        text LONGTEXT NOT NULL, 
        parent_comment_uuid CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:uuid)\', 
        created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', 
        updated_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', 
        UNIQUE INDEX UNIQ_2BC3B20DD17F50A6 (uuid), 
        INDEX IDX_2BC3B20D182A37AD (post_uuid), 
        PRIMARY KEY(uuid, post_uuid)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql(
            'ALTER TABLE 
            blog_comments 
        ADD 
            CONSTRAINT FK_2BC3B20D182A37AD FOREIGN KEY (post_uuid) REFERENCES blog_posts (uuid)');
        $this->addSql(
            'ALTER TABLE
          blog_comments
        ADD
          CONSTRAINT FK_20211221124705 FOREIGN KEY (user_uuid) REFERENCES auth_users (uuid)'
        );
        $this->addSql(
            'ALTER TABLE
          blog_comments
        ADD
          CONSTRAINT FK_20211221124706 FOREIGN KEY (parent_comment_uuid) REFERENCES blog_comments (uuid)'
        );
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE blog_comments DROP FOREIGN KEY FK_2BC3B20D182A37AD');
        $this->addSql('ALTER TABLE blog_comments DROP FOREIGN KEY FK_20211221124705');
        $this->addSql('ALTER TABLE blog_comments DROP FOREIGN KEY FK_20211221124706');
        $this->addSql('DROP TABLE blog_comments');
        $this->addSql('ALTER TABLE blog_posts DROP FOREIGN KEY FK_20211221124703');
        $this->addSql('DROP TABLE blog_posts');
    }
}
