<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211217034443 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE auth_confirmation_codes (id INT AUTO_INCREMENT NOT NULL, username VARCHAR(180) NOT NULL, code VARCHAR(255) NOT NULL, attempt INT NOT NULL, valid DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', sent_at DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', UNIQUE INDEX UNIQ_C6B3D32C77153098 (code), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE auth_role_permissions (uuid CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', resource VARCHAR(255) NOT NULL, permission VARCHAR(255) NOT NULL, PRIMARY KEY(uuid)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE auth_roles (uuid CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', name VARCHAR(255) NOT NULL, slug VARCHAR(255) NOT NULL, description LONGTEXT NOT NULL, UNIQUE INDEX UNIQ_7A1C7FB25E237E06 (name), UNIQUE INDEX UNIQ_7A1C7FB2989D9B62 (slug), PRIMARY KEY(uuid)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE auth_role_role_permissions (role_uuid CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', role_permission_uuid CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', INDEX IDX_107CEDEB6FC02232 (role_uuid), INDEX IDX_107CEDEB4F62D7AC (role_permission_uuid), PRIMARY KEY(role_uuid, role_permission_uuid)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE auth_sessions (uuid CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', user_uuid CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', refresh_token VARCHAR(255) DEFAULT NULL, client_type VARCHAR(255) DEFAULT NULL, client_build VARCHAR(255) DEFAULT NULL, client_version VARCHAR(255) DEFAULT NULL, finished_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', updated_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', PRIMARY KEY(uuid)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE auth_users (uuid CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', username VARCHAR(180) NOT NULL, phone_confirmed_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', password VARCHAR(255) NOT NULL, created_at DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', updated_at DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', UNIQUE INDEX UNIQ_D8A1F49CF85E0677 (username), PRIMARY KEY(uuid)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE auth_user_roles (user_uuid CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', role_uuid CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', INDEX IDX_4AF9D82BABFE1C6F (user_uuid), INDEX IDX_4AF9D82B6FC02232 (role_uuid), PRIMARY KEY(user_uuid, role_uuid)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE auth_role_role_permissions ADD CONSTRAINT FK_107CEDEB6FC02232 FOREIGN KEY (role_uuid) REFERENCES auth_roles (uuid) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE auth_role_role_permissions ADD CONSTRAINT FK_107CEDEB4F62D7AC FOREIGN KEY (role_permission_uuid) REFERENCES auth_role_permissions (uuid) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE auth_user_roles ADD CONSTRAINT FK_4AF9D82BABFE1C6F FOREIGN KEY (user_uuid) REFERENCES auth_users (uuid) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE auth_user_roles ADD CONSTRAINT FK_4AF9D82B6FC02232 FOREIGN KEY (role_uuid) REFERENCES auth_roles (uuid) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE auth_role_role_permissions DROP FOREIGN KEY FK_107CEDEB4F62D7AC');
        $this->addSql('ALTER TABLE auth_role_role_permissions DROP FOREIGN KEY FK_107CEDEB6FC02232');
        $this->addSql('ALTER TABLE auth_user_roles DROP FOREIGN KEY FK_4AF9D82B6FC02232');
        $this->addSql('ALTER TABLE auth_user_roles DROP FOREIGN KEY FK_4AF9D82BABFE1C6F');
        $this->addSql('DROP TABLE auth_confirmation_codes');
        $this->addSql('DROP TABLE auth_role_permissions');
        $this->addSql('DROP TABLE auth_roles');
        $this->addSql('DROP TABLE auth_role_role_permissions');
        $this->addSql('DROP TABLE auth_sessions');
        $this->addSql('DROP TABLE auth_users');
        $this->addSql('DROP TABLE auth_user_roles');
    }
}
