<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211217034444 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql(
            <<<SQL
            insert into auth_role_permissions (uuid, resource, permission)
                VALUE ('a31ccc72-3c5e-408a-83f8-46ff31755d7a', 'ADMIN_ROLE', 'ALLOWED');
            insert into auth_role_permissions (uuid, resource, permission)
                VALUE ('6e7bfebb-a087-408b-9e6f-396f811c14f2', 'ADMIN_USER', 'ALLOWED');
            insert into auth_role_permissions (uuid, resource, permission)
                VALUE ('218abe63-5856-3b43-84dd-76e1f96823e4', 'ADMIN', 'ALLOWED');
            SQL
        );
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql(
            <<<SQL
            delete from auth_role_permissions where resource = 'ADMIN_ROLE' and permission = 'ALLOWED';
            delete from auth_role_permissions where resource = 'ADMIN_USER' and permission = 'ALLOWED';
            delete from auth_role_permissions where resource = 'ADMIN' and permission = 'ALLOWED';
            SQL
        );
    }
}
