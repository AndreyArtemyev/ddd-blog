<?php

declare(strict_types=1);

namespace Tests\Blog\UI\Http\Controller;

use Exception;
use Symfony\Component\HttpFoundation\Request;
use Tests\AppWebTestCase;

class GetPostListControllerTest extends AppWebTestCase
{
    public const URI = '/api/v1/blog/post/get-list';


    public function testSuccess(): void
    {
        $this->insertDefault();

        $USER_ADMIN__phone = self::$uuidMutation[self::USER_ADMIN['username']];

        $data = $this->requestMust(
            Request::METHOD_POST,
            GetPostListControllerTest::URI,
            [
                'filter' => [
                    [
                        'command' => 'search',
                        'data' => 'статья',
                    ],
                ],
                'pagination' => [
                    'byPage' => 20,
                    'page' => 1,
                ],
            ],
            $USER_ADMIN__phone,
            false
        );

        $this->assertEqualResponse(
            $data['posts'],
            $this->getForCompareTestSuccess(),
            [
                self::ASSERT_EQUAL_RESPONSE_COLLECTION => [
                    "date_posted" => true,
                ],
            ]
        );
    }

    private function getForCompareTestSuccess(): array
    {
        return [
            [
                'uuid'=>self::$uuidMutation[self::POST_1['uuid']],
                'alias'=>self::POST_1['alias'],
                'body'=>self::POST_1['body'],
                'alias_picture_url'=>null,
                'date_posted'=>self::POST_1['date_posted'],
            ],
            [
                'uuid'=>self::$uuidMutation[self::POST_2['uuid']],
                'alias'=>self::POST_2['alias'],
                'body'=>self::POST_2['body'],
                'alias_picture_url'=>null,
                'date_posted'=>self::POST_2['date_posted'],
            ],
            [
                'uuid'=>self::$uuidMutation[self::POST_3['uuid']],
                'alias'=>self::POST_3['alias'],
                'body'=>self::POST_3['body'],
                'alias_picture_url'=>null,
                'date_posted'=>self::POST_3['date_posted'],
            ],
            [
                'uuid'=>self::$uuidMutation[self::POST_4['uuid']],
                'alias'=>self::POST_4['alias'],
                'body'=>self::POST_4['body'],
                'alias_picture_url'=>null,
                'date_posted'=>self::POST_4['date_posted'],
            ],
        ];
    }
}
