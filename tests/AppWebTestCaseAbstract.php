<?php

declare(strict_types=1);

namespace Tests;

use Doctrine\ORM\EntityManagerInterface;
use Ramsey\Uuid\Uuid;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

abstract class AppWebTestCaseAbstract extends WebTestCase
{
    private const HEADER_CONTENT_TYPE = 'CONTENT_TYPE';
    private const HEADER_CONTENT_TYPE__APPLICATION_JSON = 'application/json';
    private const HEADER_AUTHORIZATION = 'HTTP_AUTHORIZATION';
    private const HEADER_AUTHORIZATION_PREFIX = 'Bearer ';

    protected static array $uuidMutationRowIgnore = [];
    protected static array $uuidMutationRowAddToken = [];
    protected static array $uuidMutation = [];

    protected string $testToken;
    protected static KernelBrowser $client;
    protected static Application $application;
    /** @var ContainerInterface */
    protected static $container;
    protected static EntityManagerInterface $entityManagerInterface;

    public function __construct(?string $name = null, array $data = [], string $dataName = '')
    {
        /** @psalm-suppress InternalMethod */
        parent::__construct($name, $data, $dataName);
        $this->testToken = (string)($_ENV['TEST_TOKEN'] ?? '1');

        static $isGlobalInitialized;
        if ($isGlobalInitialized === null) {
            $isGlobalInitialized = true;

            self::$client = static::createClient();
            self::$container = self::$client->getContainer();
            self::$application = new Application(self::$client->getKernel());
            /** @psalm-suppress PossiblyNullReference */
            self::$entityManagerInterface = self::$container->get('doctrine')->getManager();
        }
    }

    protected function setUp(): void
    {
        static::$uuidMutation = [];
        self::$entityManagerInterface->getConnection()->beginTransaction();
    }

    protected function tearDown(): void
    {
        self::$entityManagerInterface->getConnection()->rollback();
    }


    // производит мутации ключей в рамках процесса
    protected function uuidMutation(array $data, string $table = ''): array
    {
        foreach ($data as $row => &$value) {
            if (!empty(static::$uuidMutationRowIgnore[$row])) {
                continue;
            }
            if (!empty(static::$uuidMutationRowAddToken[$table][$row])) {
                $value .= $this->testToken;
            }


            if (!isset(static::$uuidMutation[$value])) {
                if (is_string($value) && Uuid::isValid($value)) {
                    static::$uuidMutation[$value] = Uuid::uuid4()->toString();
                }

                if (is_string($value) && preg_match('|^\d{11}$|', $value) === 1) {
                    static::$uuidMutation[$value] = preg_replace(
                        '|\d{2}(\d{2})$|',
                        str_pad($this->testToken, 2, '0', STR_PAD_LEFT) . '$1',
                        $value
                    );
                }
            }


            if (isset(static::$uuidMutation[$value])) {
                $value = static::$uuidMutation[$value];
            }
        }
        unset($value);

        return $data;
    }

    /**
     * @param int|string|null $mutationValue
     * @return int|string|null
     */
    protected function getOriginValueByMutation($mutationValue)
    {
        if ($mutationValue === null) {
            return null;
        }

        foreach (static::$uuidMutation as $key => $value) {
            if ($value === $mutationValue) {
                return $key;
            }
        }

        return null;
    }

    /**
     * $ignore - для игнорирования сравнения, к примеру времени, если оно не хардкор
     * все поля из data должны быть в $mustData и наоборот
     */
    protected const ASSERT_EQUAL_RESPONSE_COLLECTION = 'collection';

    protected function assertEqualResponse(array $data, array $mustData, array $ignore = [], string $path = ''): void
    {
        foreach ($mustData as $key => $mustDatum) {
            $this->assertArrayHasKey($key, $data, $path . '/' . $key);
        }

        foreach ($data as $key => $datum) {
            $this->assertArrayHasKey($key, $mustData, $path);
            if (array_key_exists($key, $ignore) && $ignore[$key] === true) {
                continue;
            }

            if (is_array($datum)) {
                if (key_exists('uuid', $datum) && is_array(current($mustData))) {
                    foreach ($mustData as $keyData => $item) {
                        if ($item['uuid'] === $datum['uuid']) {
                            $key = $keyData;
                            break;
                        }
                    }
                }
                $this->assertEqualResponse(
                    $datum,
                    $mustData[$key],
                    $ignore[self::ASSERT_EQUAL_RESPONSE_COLLECTION] ?? $ignore[$key] ?? [],
                    $path . '/' . $key
                );
            } else {
                $message = $path . '/' . $key;
                if (($mutation = $this->getOriginValueByMutation($mustData[$key])) !== null) {
                    $message .= PHP_EOL . ' mutation must: ' . $mutation;
                }
                if (($mutation = $this->getOriginValueByMutation($datum)) !== null) {
                    $message .= PHP_EOL . ' mutation: ' . $mutation;
                }

                $this->assertSame(
                    $mustData[$key],
                    $datum,
                    $message
                );
            }
        }
    }


    // положительный ответ, возвращает результат, может быть null|array
    protected function requestMust(
        string $method,
        string $uri,
        array $parameters,
        string $userPhone,
        bool $isAuth = true
    ): ?array {
        $parsesResponseContent = $this->requestJson($method, $uri, $parameters, $userPhone, $isAuth);

        $this->assertIsBool($parsesResponseContent['success']);
        $this->assertNull($parsesResponseContent['error']);

        return $parsesResponseContent['data'];
    }

    // отрицательный ответ, возвращает ошибку
    protected function requestErrorMust(string $method, string $uri, array $parameters, string $userPhone): array
    {
        $parsesResponseContent = $this->requestJson($method, $uri, $parameters, $userPhone);

        $this->assertNull($parsesResponseContent['data']);
        $this->assertIsBool($parsesResponseContent['success']);
        $this->assertIsArray($parsesResponseContent['error']);

        return $parsesResponseContent['error'];
    }

    // проверка только структуры ответа, транспортный уровень проверяется только на code === 200
    protected function requestJson(
        string $method,
        string $uri,
        array $parameters,
        string $userPhone,
        bool $isAuth = true
    ): array {
        $parsesResponseContent = $this->parseResponseContent(
            $this->request(
                $method,
                $uri,
                $parameters,
                [],
                $userPhone,
                [
                    'CONTENT_TYPE' => 'application/json',
                ],
                $isAuth
            )
        );
        $this->assertIsArray($parsesResponseContent);
        $this->assertArrayHasKey('success', $parsesResponseContent);
        $this->assertArrayHasKey('error', $parsesResponseContent);
        $this->assertArrayHasKey('data', $parsesResponseContent);

        return $parsesResponseContent;
    }

    protected function requestHTML(string $method, string $uri, array $parameters, string $userPhone): string
    {
        $response = $this
            ->request(
                $method,
                $uri,
                $parameters,
                [],
                $userPhone,
                [
                    'CONTENT_TYPE' => 'text/html',
                ]
            )
            ->getResponse();
        $this->assertEquals(200, $response->getStatusCode());

        $content = $response->getContent();
        $this->assertIsString($content);

        return $content;
    }

    protected function requestBinaryMust(string $method, string $uri, array $parameters, string $userPhone): string
    {
        $response = $this
            ->request(
                $method,
                $uri,
                $parameters,
                [],
                $userPhone,
                []
            )
            ->getResponse();
        $this->assertEquals(200, $response->getStatusCode());

        $content = $response->getContent();
        $this->assertIsString($content);
        $this->assertNotEmpty($content);

        return $content;
    }


    protected function requestMultipartFormDataMust(
        string $method,
        string $uri,
        array $parameters,
        array $files,
        string $userPhone
    ): ?array {
        $parsesResponseContent = $this->parseResponseContent(
            $this->request(
                $method,
                $uri,
                $parameters,
                $files,
                $userPhone,
                [
                    'CONTENT_TYPE' => 'multipart/form-data',
                ]
            )
        );
        $this->assertIsArray($parsesResponseContent);
        $this->assertArrayHasKey('success', $parsesResponseContent);
        $this->assertArrayHasKey('error', $parsesResponseContent);
        $this->assertArrayHasKey('data', $parsesResponseContent);
        $this->assertIsBool($parsesResponseContent['success']);
        $this->assertNull($parsesResponseContent['error']);

        return $parsesResponseContent['data'];
    }

    protected function request(
        string $method,
        string $uri,
        array $parameters,
        array $files,
        string $userPhone,
        array $headers,
        bool $isAuth = true
    ): KernelBrowser {
        if ($isAuth) {
            $token = $this->getToken($userPhone);
            $headers = array_merge(
                $headers,
                [
                    self::HEADER_AUTHORIZATION => self::HEADER_AUTHORIZATION_PREFIX . $token,
                ]
            );
        }

        $client = clone self::$client;
        $client->request(
            $method,
            $uri,
            $parameters,
            $files,
            $headers
        );

        return $client;
    }


    protected function getToken(string $phone): string
    {
        if (strlen($phone) === 11 && substr($phone, 0, 1) === '7') {
            $phone = substr($phone, 1, strlen($phone) - 1);
        }

        $client = clone self::$client;
        $e = $client->request(
            Request::METHOD_POST,
            '/api/v1/auth/send-code',
            [
                'phone' => $phone,
                'country_code' => 'RU',
            ],
            [],
            [
                self::HEADER_CONTENT_TYPE => self::HEADER_CONTENT_TYPE__APPLICATION_JSON,
            ]
        );

        $client = clone self::$client;
        $client->request(
            Request::METHOD_POST,
            '/api/v1/auth/confirm',
            [
                'phone' => $phone,
                'country_code' => 'RU',
                'code' => 5555,
            ],
            [],
            [
                self::HEADER_CONTENT_TYPE => self::HEADER_CONTENT_TYPE__APPLICATION_JSON,
            ]
        );

        $parsesResponseContent = $this->parseResponseContent($client);
        $this->assertIsArray($parsesResponseContent);
        $this->assertArrayHasKey('data', $parsesResponseContent);
        $this->assertArrayHasKey('token', $parsesResponseContent['data']);
        $this->assertNotEmpty($parsesResponseContent['data']['token']);
        return $parsesResponseContent['data']['token'];
    }

    /**
     * @param KernelBrowser $client
     * @return array|false
     */
    protected function parseResponseContent(KernelBrowser $client)
    {
        return json_decode((string)$client->getResponse()->getContent(), true);
    }

    protected function mutationInsert(string $table, array $data, array $types = []): void
    {
        self::$entityManagerInterface->getConnection()->insert($table, $this->uuidMutation($data, $table));
    }

    protected function mutationUpdate(string $table, array $data, array $criteria, array $types = []): void
    {
        self::$entityManagerInterface->getConnection()->update(
            $table,
            $this->uuidMutation($data, $table),
            $this->uuidMutation($criteria, $table),
            $types
        );
    }
}
