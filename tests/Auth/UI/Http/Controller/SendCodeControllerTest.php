<?php

declare(strict_types=1);

namespace Tests\Auth\UI\Http\Controller;

use Symfony\Component\HttpFoundation\Request;
use Tests\AppWebTestCase;

class SendCodeControllerTest extends AppWebTestCase
{
    public const URI = '/api/v1/auth/send-code';


    public function testSuccess(): void
    {
        $this->insertDefault();
        $this->insertDefaultAdmin();

        $USER_ADMIN__phone = self::$uuidMutation[self::USER_ADMIN['username']];

        $data = $this->requestMust(
            Request::METHOD_POST,
            SendCodeControllerTest::URI,
            [
                'phone' => $USER_ADMIN__phone,
                'country_code' => 'RU',
            ],
            $USER_ADMIN__phone,
            false
        );

        $this->assertNull($data);
    }
}
