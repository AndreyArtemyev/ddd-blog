<?php

declare(strict_types=1);

namespace Tests;

use App\Auth\Domain\Entity\Role;
use App\Auth\Domain\Entity\User;
use App\Common\Lib\Helpers\DateTimeHelper;
use DateTimeImmutable;

class AppWebTestCase extends AppWebTestCaseAbstract
{
    protected static array $uuidMutationRowIgnore = [
        'role_permission_uuid' => true,
    ];
    protected static array $uuidMutationRowAddToken = [
        'auth_roles' => [
            'name' => true,
            'slug' => true,
        ],
    ];

    public function __construct(?string $name = null, array $data = [], string $dataName = '')
    {
        parent::__construct($name, $data, $dataName);

        static $isGlobalInitialized;
        if ($isGlobalInitialized === null) {
            $isGlobalInitialized = true;

            $this->initAuthBundleRole();
        }
    }

    private function initAuthBundleRole(): void
    {
        self::$ROLE_PERMISSION__ADMIN__UUID                        = self::$entityManagerInterface->getConnection()->executeQuery(
            <<<SQL
            select uuid from auth_role_permissions where
            resource = 'ADMIN' and permission = 'ALLOWED';
            SQL
        )->fetchOne();
        self::$ROLE_ROLE_PERMISSION__ADMIN['role_permission_uuid'] = self::$ROLE_PERMISSION__ADMIN__UUID;

        self::$ROLE_PERMISSION__ADMIN_ROLE__UUID                        = self::$entityManagerInterface->getConnection()->executeQuery(
            <<<SQL
            select uuid from auth_role_permissions where
            resource = 'ADMIN_ROLE' and permission = 'ALLOWED';
            SQL
        )->fetchOne();
        self::$ROLE_ROLE_PERMISSION__ADMIN_ROLE['role_permission_uuid'] = self::$ROLE_PERMISSION__ADMIN_ROLE__UUID;

        self::$ROLE_PERMISSION__ADMIN_USER__UUID                        = self::$entityManagerInterface->getConnection()->executeQuery(
            <<<SQL
            select uuid from auth_role_permissions where
            resource = 'ADMIN_USER' and permission = 'ALLOWED';
            SQL
        )->fetchOne();
        self::$ROLE_ROLE_PERMISSION__ADMIN_USER['role_permission_uuid'] = self::$ROLE_PERMISSION__ADMIN_USER__UUID;
    }

    protected function insertDefault(): void
    {
        $this->insertUserDefault();
        $this->insertPostDefault();
        $this->insertCommentDefault();
    }

    protected function insertUserDefault(): void
    {
        $this->insertUser(self::USER_ADMIN);
    }

    protected function insertDefaultAdmin(): void
    {
        $this->insertRole(self::ROLE__ADMIN);
        $this->insertRoleRolePermission(self::$ROLE_ROLE_PERMISSION__ADMIN);
        $this->insertUserRole(self::USER_ROLE__ADMIN);
    }

    protected function insertUser(array $data): void
    {
        $data['password']           = (string)\password_hash($data['username'], PASSWORD_BCRYPT, ['cost' => 12]);
        $data['phone_confirmed_at'] = (bool)rand(0, 1) ? '2001-01-20 10:31:42' : null;
        $this->mutationInsert(User::TABLE_NAME, $data);
    }

    protected function insertRole(array $data): void
    {
        $data['slug']        = $data['name'];
        $data['description'] = '';

        $this->mutationInsert(Role::TABLE_NAME, $data);
    }

    protected function insertRoleRolePermission(array $data): void
    {
        $this->mutationInsert(Role::TABLE_NAME_ROLE_PERMISSION, $data);
    }

    protected function insertUserRole(array $data): void
    {
        $this->mutationInsert(User::TABLE_NAME_ROLE, $data);
    }

    protected function insertPostDefault(): void
    {
        $this->insertPost(self::POST_1);
        $this->insertPost(self::POST_2);
        $this->insertPost(self::POST_3);
        $this->insertPost(self::POST_4);
    }

    protected function insertCommentDefault(): void
    {
        $this->insertComment(self::COMMENT_1);
        $this->insertComment(self::COMMENT_2);
        $this->insertComment(self::COMMENT_3);
        $this->insertComment(self::COMMENT_4);
    }

    protected function insertPost(array $data): void
    {
        $data['created_at'] = DateTimeHelper::forFrontMust(new DateTimeImmutable());
        $data['updated_at'] = DateTimeHelper::forFrontMust(new DateTimeImmutable());

        $this->mutationInsert('blog_posts', $data);
    }

    protected function insertComment(array $data): void
    {
        $data['created_at'] = DateTimeHelper::forFrontMust(new DateTimeImmutable());
        $data['updated_at'] = DateTimeHelper::forFrontMust(new DateTimeImmutable());

        $this->mutationInsert('blog_comments', $data);
    }

    protected const USER_ADMIN = [
        'uuid' => '7b996c61-db17-44d0-8590-aef8396d096b',
        'username' => '79000000000',
    ];

    protected const ROLE__ADMIN = [
        'uuid' => '3acad371-adce-4d8b-86d4-86702bce21bd',
        'name' => 'Admin',
    ];
    protected const ROLE__ADMIN_ROLE = [
        'uuid' => '78ae6b5e-19cc-4f65-951c-8a399a93e267',
        'name' => 'Full access to roles',
    ];
    protected const ROLE__ADMIN_USER = [
        'uuid' => '1bf65637-15f6-4cf3-a827-036f94f4c2c5',
        'name' => 'Full access to users',
    ];

    protected const POST_1 = [
        'uuid' => '375d2a74-46ab-d54f-8071-9a7240ac5ded',
        'user_uuid' => self::USER_ADMIN['uuid'],
        'alias' => 'Тестовая статья 1',
        'body' => 'Тестовая статья Тестовая статья Тестовая статья Тестовая статья 1',
        'alias_picture_uuid' => null,
        'date_posted' => '2021-11-23 3:40:42',
    ];
    protected const POST_2 = [
        'uuid' => 'c8065fa5-d72d-0344-9c0b-6b3d58e74e5b',
        'user_uuid' => self::USER_ADMIN['uuid'],
        'alias' => 'Тестовая статья 2',
        'body' => 'Тестовая статья Тестовая статья Тестовая статья Тестовая статья 2',
        'alias_picture_uuid' => null,
        'date_posted' => '2021-11-23 3:40:42',
    ];
    protected const POST_3 = [
        'uuid' => 'df119189-82cc-d94e-a1c6-d3accff4873c',
        'user_uuid' => self::USER_ADMIN['uuid'],
        'alias' => 'Тестовая статья 3',
        'body' => 'Тестовая статья Тестовая статья Тестовая статья Тестовая статья 3',
        'alias_picture_uuid' => null,
        'date_posted' => '2021-11-23 3:40:42',
    ];
    protected const POST_4 = [
        'uuid' => 'c0155fe2-fe98-ef4a-8a2b-736339fda073',
        'user_uuid' => self::USER_ADMIN['uuid'],
        'alias' => 'Тестовая статья 4',
        'body' => 'Тестовая статья Тестовая статья Тестовая статья Тестовая статья 4',
        'alias_picture_uuid' => null,
        'date_posted' => '2021-11-23 3:40:42',
    ];

    protected const COMMENT_1 = [
        'uuid' => 'c0155fe2-fe98-ef4a-8a2b-736339fda073',
        'post_uuid' => self::POST_1['uuid'],
        'user_uuid' => self::USER_ADMIN['uuid'],
        'text' => 'Тестовый комментарий 1',
        'parent_comment_uuid' => null,
    ];

    protected const COMMENT_2 = [
        'uuid' => '660ab83a-08fc-654d-8950-7bc5f4e37809',
        'post_uuid' => self::POST_1['uuid'],
        'user_uuid' => self::USER_ADMIN['uuid'],
        'text' => 'Тестовый комментарий 2',
        'parent_comment_uuid' => null,
    ];

    protected const COMMENT_3 = [
        'uuid' => '5cdfa6d6-7fca-3347-b585-b07795bccc04',
        'post_uuid' => self::POST_1['uuid'],
        'user_uuid' => self::USER_ADMIN['uuid'],
        'text' => 'Тестовый комментарий 3',
        'parent_comment_uuid' => self::COMMENT_2['uuid'],
    ];

    protected const COMMENT_4 = [
        'uuid' => '33a1c324-5233-5d48-98ca-edf327560eb1',
        'post_uuid' => self::POST_1['uuid'],
        'user_uuid' => self::USER_ADMIN['uuid'],
        'text' => 'Тестовый комментарий 4',
        'parent_comment_uuid' => self::COMMENT_2['uuid'],
    ];

    protected const USER_ROLE__ADMIN = [
        'user_uuid' => self::USER_ADMIN['uuid'],
        'role_uuid' => self::ROLE__ADMIN['uuid'],
    ];
    protected const USER_ROLE__ADMIN_ROLE = [
        'user_uuid' => self::USER_ADMIN['uuid'],
        'role_uuid' => self::ROLE__ADMIN_ROLE['uuid'],
    ];
    protected const USER_ROLE__ADMIN_USER = [
        'user_uuid' => self::USER_ADMIN['uuid'],
        'role_uuid' => self::ROLE__ADMIN_USER['uuid'],
    ];

    protected static string $ROLE_PERMISSION__ADMIN__UUID;
    protected static array $ROLE_ROLE_PERMISSION__ADMIN = [
        'role_uuid' => self::ROLE__ADMIN['uuid'],
        'role_permission_uuid' => '',
    ];
    protected static string $ROLE_PERMISSION__ADMIN_ROLE__UUID;
    protected static array $ROLE_ROLE_PERMISSION__ADMIN_ROLE = [
        'role_uuid' => self::ROLE__ADMIN_ROLE['uuid'],
        'role_permission_uuid' => '',
    ];
    protected static string $ROLE_PERMISSION__ADMIN_USER__UUID;
    protected static array $ROLE_ROLE_PERMISSION__ADMIN_USER = [
        'role_uuid' => self::ROLE__ADMIN_USER['uuid'],
        'role_permission_uuid' => '',
    ];
}