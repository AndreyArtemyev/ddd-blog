#!/usr/bin/env sh

envsubst "
\${COMPOSE_PROJECT_NAME}
\${APP_NOT_USE_END}" < "/etc/nginx/conf.d/default.conf.dist" > "/etc/nginx/conf.d/default.conf"

nginx -g 'daemon off;'
