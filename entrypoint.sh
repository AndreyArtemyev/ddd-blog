#!/usr/bin/env sh

composer run-script --no-cache post-install-cmd
bin/console common:create:buckets
bin/console doctrine:migrations:migrate
chown -R www-data:www-data var

php-fpm