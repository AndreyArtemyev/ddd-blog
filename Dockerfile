FROM php:7.4-fpm

RUN apt-get update

RUN docker-php-ext-install bcmath

RUN apt-get install -y \
    libjpeg62-turbo-dev \
    libpng-dev \
    libfreetype6-dev \
        && docker-php-ext-configure gd --with-freetype --with-jpeg \
        && docker-php-ext-install -j$(nproc) gd

RUN apt-get install -y \
    libpq-dev \
        && docker-php-ext-install -j$(nproc) pdo pdo_mysql pdo_pgsql

RUN apt-get install -y \
    libxml2-dev \
        && docker-php-ext-install soap

RUN docker-php-ext-install -j$(nproc) sockets

RUN apt-get install -y \
    libzip-dev \
        && docker-php-ext-install zip

RUN apt-get install -y \
    libmagickwand-dev \
        && pecl install imagick \
        && docker-php-ext-enable imagick

RUN pecl install redis \
        && docker-php-ext-enable redis

RUN pecl install mongodb \
        && docker-php-ext-enable mongodb

RUN docker-php-ext-enable opcache

RUN apt-get install -y \
    librabbitmq-dev \
        && pecl install amqp \
        && docker-php-ext-enable amqp

RUN apt-get install -y libicu-dev \
    && docker-php-ext-configure intl \
    && docker-php-ext-install intl

RUN mkdir -p /var/log/php-fpm
RUN ln -sf /dev/stdout /var/log/php-fpm/access.log
RUN ln -sf /dev/stderr /var/log/php-fpm/error.log

# install git
RUN apt-get update && \
    apt-get upgrade -y && \
    apt-get install -y git

#RUN apt-get install -y --no-install-recommends git

RUN curl -sS https://getcomposer.org/installer | php -- --version 2.0.9 --install-dir=/usr/local/bin --filename=composer

WORKDIR /var/www

ARG COMPOSER_AUTH
ENV COMPOSER_AUTH ${COMPOSER_AUTH}


COPY . .
RUN composer install --no-cache  --no-scripts
ENTRYPOINT ["./entrypoint.sh"]
